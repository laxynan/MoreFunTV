//
//  JAInternationalControl.m
//  JASidePanels
//
//  Created by 才沈 on 14-8-8.
//
//

#import "JAInternationalControl.h"
// 创建静态变量bundle
static NSBundle *bundle = nil;

@implementation JAInternationalControl

// 获取bundle方法
+(NSBundle *)bundle {
    return bundle;
}

// 初始化方法：userLanguage存储在NSUserDefaults中，首次加载时要检测是否存在，如果不存在的话读AppleLanguages，并附值给userLanguage。
+(void)initUserLanguage {
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *string = @"zh-Hans";
    // 获取系统当前语言版本（中文zh-Hans，英文en）
    NSArray *languages = [def objectForKey:@"AppleLanguages"];
    NSString *current = [languages objectAtIndex:0];
    string = current;
    if (![string isEqualToString:@"zh-Hans"]) {
        string = @"en";
    }
    [def setValue:string forKey:@"userLanguage"];
    [def synchronize];  // 持久化，不加的话不会保存
    // 获取文件路径
    NSString *path = [[NSBundle mainBundle] pathForResource:string ofType:@"lproj"];
    bundle = [NSBundle bundleWithPath:path];    // 生成bundle
}

// 初始化方法：userLanguage存储在NSUserDefaults中，首次加载时要检测是否存在，如果不存在的话读AppleLanguages，并附值给userLanguage。
+(void)initUserLanguage0 {
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *string = [def valueForKey:@"userLanguage"];
    if (string.length == 0) {
        // 获取系统当前语言版本（中文zh-Hans，英文en）
        NSArray *languages = [def objectForKey:@"AppleLanguages"];
        NSString *current = [languages objectAtIndex:0];
        string = current;
        [def setValue:current forKey:@"userLanguage"];
        [def synchronize];  // 持久化，不加的话不会保存
    }
    // 获取文件路径
    NSString *path = [[NSBundle mainBundle] pathForResource:string ofType:@"lproj"];
    bundle = [NSBundle bundleWithPath:path];    // 生成bundle
}

// 获得当前语言方法
+(NSString *)userLanguage {
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *language = [def valueForKey:@"userLanguage"];
    return language;
}

// 设置语言方法
+(void)setUserLanguage:(NSString *)language {
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    // 1.改变bundle的值
    NSString *path = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
    bundle = [NSBundle bundleWithPath:path];
    // 2.持久化
    [def setValue:language forKey:@"userLanguage"];
    [def synchronize];
}

@end
