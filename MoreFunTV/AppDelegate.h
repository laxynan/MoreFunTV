//
//  AppDelegate.h
//  MoreFunTV
//
//  Created by admin on 14-10-21.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "ClassViewController.h"
#import "SearchViewController.h"
#import "PersonalViewController.h"
#import "SetUpSingleton.h"
#import "NetWorkingHelper.h"
#import "DBDaoHelper.h"
#import "MatchstickDeviceController.h"
#import <Matchstick/Flint.h>
#import "DBDaoHelper.h"
#import "ASIDownloadCache.h"
#import "JGActionSheet.h"
#import "SetUpSingleton.h"
#import "Reachability.h"

#include <sys/types.h>
#include <sys/param.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <net/if_dl.h>
#include <sys/sysctl.h>
#import "RCDraggableButton.h"
#import <MediaPlayer/MediaPlayer.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,MSFKLoggerDelegate,JGActionSheetDelegate,MatchstickControllerDelegate,UIGestureRecognizerDelegate>
{
    SetUpSingleton *_setuuuuP;
}
@property (retain, nonatomic) UIWindow *window;
@property (nonatomic,retain) Reachability *hostReach;//网络判断
@property (nonatomic,retain) ASIDownloadCache *myCache;//缓存
@property (nonatomic,retain) RCDraggableButton *avatar;
@property MatchstickDeviceController *matchstickDeviceController;

#ifdef DEBUG
@property (retain, nonatomic) UILabel *memoryLabel;
#endif

@end
