//
//  NetWorkingHelper.m
//  MoreFunTV
//
//  Created by admin on 14-10-30.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "NetWorkingHelper.h"

@implementation NetWorkingHelper
//获取推荐信息数据
+ (NSMutableArray *)getRecommendInfoWithRequestParameter:(RequestParameter *)reqParameter
{
    //获取数据
    NSDictionary *resultDic = [NetWorking sendRequestWithUrlJsonGETParameter:reqParameter];
    //解析数据
    NSDictionary *dicData = [resultDic objectForKey:@"data"];
    //判断数据是否为空
    if (![dicData isKindOfClass:[NSNull class]] && dicData.count > 0)
    {//字典不为空
        NSMutableArray *arrVideoIfo = [[NSMutableArray alloc]init];
        NSArray *arrItems = [dicData objectForKey:@"items"];
        for (int i = 0; i < arrItems.count; i++)
        {
            NSDictionary *dicVideoIfo = [arrItems objectAtIndex:i];
            VideoInfo *vInfo = [[VideoInfo alloc]init];
            vInfo.strVideoId = [dicVideoIfo objectForKey:@"id"];
            vInfo.strCategoryId = [dicVideoIfo objectForKey:@"categoryId"];
            vInfo.strCategoryName = [dicVideoIfo objectForKey:@"categoryName"];
            vInfo.strTitle = [dicVideoIfo objectForKey:@"title"];
            vInfo.strThumbnail = [dicVideoIfo objectForKey:@"thumbnail"];
            vInfo.strReleasedEpisodes = [dicVideoIfo objectForKey:@"releasedEpisodes"];
            
            [arrVideoIfo addObject:vInfo];
        }
        return arrVideoIfo;
    }
    else
    {//字典为空
        return nil;
    }
}

//获取单个分类的详细信息
+ (NSMutableArray *)getCategoryVoideInfoWithRequestParameter:(RequestParameter *)reqParameter
{
    //获取数据
    NSDictionary *resultDic = [NetWorking sendRequestWithUrlJsonGETParameter:reqParameter];
    if (![resultDic isKindOfClass:[NSNull class]] && resultDic.count > 0) {
    }else{
        return nil;
    }
    
    //解析数据
    NSDictionary *dicData = [resultDic objectForKey:@"data"];
    //判断数据是否为空
    if (![dicData isKindOfClass:[NSNull class]] && dicData.count > 0)
    {//字典不为空
        NSArray *arrItems = [dicData objectForKey:@"items"];
        NSMutableArray *arrVoide = [[NSMutableArray alloc]init];
        for (int i = 0; i < arrItems.count; i++)
        {
            NSDictionary *dicVideoIfo = [arrItems objectAtIndex:i];
            VideoInfo *vInfo = [[VideoInfo alloc]init];
            vInfo.strVideoId = [dicVideoIfo objectForKey:@"id"];
            vInfo.strCategoryId = [dicVideoIfo objectForKey:@"categoryId"];
            vInfo.strCategoryName = [dicVideoIfo objectForKey:@"categoryName"];
            vInfo.strTitle = [dicVideoIfo objectForKey:@"title"];
            vInfo.strThumbnail = [dicVideoIfo objectForKey:@"thumbnail"];
            vInfo.strReleasedEpisodes = [dicVideoIfo objectForKey:@"releasedEpisodes"];
            
            [arrVoide addObject:vInfo];
        }
        //单例
        SetUpSingleton *set = [SetUpSingleton sharedSetUp];
        set.pageNumber = [[dicData objectForKey:@"totalPages"] intValue];
        return arrVoide;
    }
    else
    {//字典为空
        return nil;
    }
}

//获取直播信息数据
+ (NSMutableArray *)getLiveInfoWithRequestParameter:(RequestParameter *)reqParameter
{
    NSDictionary *resultDic = [NetWorking sendRequestWithUrlJsonGETParameter:reqParameter];
    NSDictionary *dicData = [resultDic objectForKey:@"data"];
    if (![dicData isKindOfClass:[NSNull class]] && dicData.count > 0)
    {
        if (reqParameter.flagPage == 10000) {
            NSMutableArray *arrLive = [[NSMutableArray alloc]init];
            NSArray *arrItems = [dicData objectForKey:@"items"];
            for (int i = 0; i < arrItems.count; i++)
            {
                NSDictionary *dicVideoIfo = [arrItems objectAtIndex:i];
                LiveInfo *live = [[LiveInfo alloc]init];
                live.strLiveId = [dicVideoIfo objectForKey:@"id"];
                live.strPlayUrl = [dicVideoIfo objectForKey:@"channelUrl"];
                live.strResTitle = [dicVideoIfo objectForKey:@"title"];
                live.strThumbnail = [dicVideoIfo objectForKey:@"thumbnail"];
                live.flag = reqParameter.flag;
                
                [DBDaoHelper insertLiveIDWith:live];
                [arrLive addObject:live];
            }
            return arrLive;
        }
        else if (reqParameter.flagPage == 100)
        {
            //单例
            SetUpSingleton *set = [SetUpSingleton sharedSetUp];
            set.pageWeiShiNumber = [[dicData objectForKey:@"totalPages"] intValue];
        }
        else{
            SetUpSingleton *setUp = [SetUpSingleton sharedSetUp];
            setUp.pageDiFangNumber = [[dicData objectForKey:@"totalPages"] intValue];
        }
    }
    return nil;
}

//获取直播EPG信息数据 并存入数据库
+(void)getDetailEPGInfoWith:(RequestParameter *)rep ArrLive:(NSMutableArray *)arrLive
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateFrom = [[NSDate alloc]init];//—方法＋时差的时间
    rep.strStartOnFrom = [formatter stringFromDate:dateFrom];
    NSDate *dateTo = [[NSDate alloc]initWithTimeIntervalSinceNow:24*60*60];//—方法＋时差的时间
    rep.strStartOnTo = [formatter stringFromDate:dateTo];
    
    rep.portID = 8;
    for (int j = 0; j < arrLive.count; j++)
    {
        LiveInfo *live = [arrLive objectAtIndex:j];
        rep.strResId = live.strLiveId;
        
        NSMutableArray *arr =[self getLiveEPGWithRep:rep];
        for (int i = 0; i < arr.count; i++)
        {
            LiveInfo *liveIfo = [arr objectAtIndex:i];
            if ([liveIfo.strResId intValue] == [live.strLiveId intValue]) {
                liveIfo.strPlayUrl = live.strPlayUrl;
                [DBDaoHelper insertEPGWith:liveIfo];
            }
        }
    }
}


//获取直播EPG信息数据
+ (NSMutableArray *)getLiveEPGWithRep:(RequestParameter *)reqParameter
{
    NSDictionary *resultDic = [NetWorking sendRequestWithUrlJsonGETParameter:reqParameter];
    NSDictionary *dicData = [resultDic objectForKey:@"data"];
    if (![dicData isKindOfClass:[NSNull class]] && dicData.count > 0)
    {
        NSArray *arrItems = [dicData objectForKey:@"items"];
        NSMutableArray *arrEPG = [[NSMutableArray alloc]init];
        for (int i = 0; i < arrItems.count; i++)
        {
            NSDictionary *dicVideoIfo = [arrItems objectAtIndex:i];
            LiveInfo *liveInfo = [[LiveInfo alloc]init];
            liveInfo.strStartOn = [dicVideoIfo objectForKey:@"startOn"];
            liveInfo.strId = [dicVideoIfo objectForKey:@"id"];
            liveInfo.strTitle = [dicVideoIfo objectForKey:@"title"];
            liveInfo.strEndOn = [dicVideoIfo objectForKey:@"endOn"];
            liveInfo.strThumbnail = [dicVideoIfo objectForKey:@"resThumbnail"];
            liveInfo.strResId = [dicVideoIfo objectForKey:@"resId"];
            liveInfo.strResTitle = [dicVideoIfo objectForKey:@"resTitle"];
            
            [arrEPG addObject:liveInfo];
        }
        return arrEPG;
    }
    return nil;
}


//点击按钮搜索 请求数据
+ (NSMutableArray *)getSearchBtnWithRep:(RequestParameter *)reqParameter
{
    NSDictionary *resultDic = [NetWorking sendRequestWithUrlJsonGETParameter:reqParameter];
    NSDictionary *dicData = [resultDic objectForKey:@"data"];
    if (![dicData isKindOfClass:[NSNull class]] && dicData.count > 0)
    {
        NSArray *arrItems = [dicData objectForKey:@"items"];
        NSMutableArray *arrVoide = [[NSMutableArray alloc]init];
        for (int i = 0; i < arrItems.count; i++)
        {
            NSDictionary *dicVideoIfo = [arrItems objectAtIndex:i];
            Search *searchInfo = [[Search alloc]init];
            searchInfo.strId = [dicVideoIfo objectForKey:@"id"];
            searchInfo.strCategoryName = [dicVideoIfo objectForKey:@"categoryName"];
            searchInfo.strTitle = [dicVideoIfo objectForKey:@"title"];
            searchInfo.strDescription = [dicVideoIfo objectForKey:@"description"];
            searchInfo.strThumbnail = [dicVideoIfo objectForKey:@"thumbnail"];
            searchInfo.flag = 111;
            [arrVoide addObject:searchInfo];
        }
        return arrVoide;
    }
    else
    {
        return nil;
    }
}

//联想搜索 请求数据
+ (NSMutableArray *)getSearchWithWords:(NSString *)words
{
    NSDictionary *resultDic = [NetWorking sendSearchWithURL:words];
    NSArray *arrData = [resultDic objectForKey:@"r"];
    if (![arrData isKindOfClass:[NSNull class]] && arrData.count > 0)
    {
        NSMutableArray *arrVoide = [[NSMutableArray alloc]init];
        for (int i = 0; i < arrData.count; i++)
        {
            NSDictionary *dicVideoIfo = [arrData objectAtIndex:i];
            Search *searchInfo = [[Search alloc]init];
            searchInfo.strTitle = [dicVideoIfo objectForKey:@"c"];
            searchInfo.flag = 88;
            
            [arrVoide addObject:searchInfo];
        }
        return arrVoide;
    }
    else
    {
        return nil;
    }
}

//热词查询详情
+ (NSMutableArray *)getHotWordsWithRequestParameter:(RequestParameter *)reqParameter
{
    NSDictionary *resultDic = [NetWorking sendRequestWithUrlJsonGETParameter:reqParameter];
    NSDictionary *dicData = [resultDic objectForKey:@"data"];
    if (![dicData isKindOfClass:[NSNull class]] && dicData.count > 0)
    {
        NSArray *arrItems = [dicData objectForKey:@"items"];
        NSMutableArray *arrVoide = [[NSMutableArray alloc]init];
        for (int i = 0; i < arrItems.count; i++)
        {
            NSDictionary *dicVideoIfo = [arrItems objectAtIndex:i];
            HotWords *hot = [[HotWords alloc]init];
            hot.strHotWordsId = [dicVideoIfo objectForKey:@"id"];
            hot.strHotWordsName = [dicVideoIfo objectForKey:@"hotword"];
            
            [arrVoide addObject:hot];
        }
        return arrVoide;
    }
    else
    {
        return nil;
    }
}


//获取类别信息
+ (NSMutableArray *)getClassInfoWith:(NSString *)name
{
    NSMutableArray *arrResult = [NetWorking sendRequestClassInfoWithTypeName:name];
    if (![arrResult isKindOfClass:[NSNull class]] && arrResult.count > 0)
    {
        NSMutableArray *arrClass = [[NSMutableArray alloc]init];
        for (int i = 0; i < arrResult.count; i++)
        {
            NSDictionary *dicClass = [arrResult objectAtIndex:i];
            VideoInfo *class = [[VideoInfo alloc]init];
            class.strCategoryId = [dicClass objectForKey:@"id"];
            class.strCategoryName = [dicClass objectForKey:@"name"];
            class.strCategoryLog = [dicClass objectForKey:@"icon"];
            class.strCategoryEn = [dicClass objectForKey:@"en"];
            
            [arrClass addObject:class];
        }
        return arrClass;
    }
    else
    {
        return nil;
    }
}

//获取类别下的条件选项信息
+ (NSMutableArray *)getTypeInfoWithTypeName:(NSString *)name
{
    NSMutableArray *arrResult = [NetWorking sendRequestClassInfoWithTypeName:name];
    if (![arrResult isKindOfClass:[NSNull class]] && arrResult.count > 0)
    {
        NSMutableArray *arrType = [[NSMutableArray alloc]init];
        for (int i = 0; i < arrResult.count; i++)
        {
            NSDictionary *dicClass = [arrResult objectAtIndex:i];
            VideoInfo *class = [[VideoInfo alloc]init];
            class.strCategoryId = [dicClass objectForKey:@"id"];
            class.strCategoryName = [dicClass objectForKey:@"name"];
            [arrType addObject:class];
        }
        return arrType;
    }
    else
    {
        return nil;
    }
}

//获取客户端信息
+ (Client *)getClientInfoWithRep:(RequestParameter *)reqParameter
{
    NSDictionary *resultDic = [NetWorking sendRequestWithUrlJsonGETParameter:reqParameter];
    NSDictionary *dicData = [resultDic objectForKey:@"data"];
    if (![dicData isKindOfClass:[NSNull class]] && dicData.count > 0)
    {
        Client *cl = [[Client alloc]init];
        cl.strClientId = [dicData objectForKey:@"id"];
        cl.strEncryptKey = [dicData objectForKey:@"encryptKey"];
        cl.strRootAppId = [dicData objectForKey:@"rootAppId"];
        cl.strVersion = [dicData objectForKey:@"version"];
        
        return cl;
    }
    else{
        return nil;
    }
}

//获取根应用信息
+ (NSMutableArray *)getSubappsInfoWithRep:(RequestParameter *)reqParameter
{
    NSDictionary *resultDic = [NetWorking sendRequestWithUrlJsonGETParameter:reqParameter];
    NSDictionary *dicData = [resultDic objectForKey:@"data"];
    if (![dicData isKindOfClass:[NSNull class]] && dicData.count > 0)
    {
        NSArray *arrItems = [dicData objectForKey:@"items"];
        NSMutableArray *arrSubapp = [[NSMutableArray alloc]init];
        for (int i = 0; i < arrItems.count; i++)
        {
            NSDictionary *dicSubapp = [arrItems objectAtIndex:i];
            Subapps *su = [[Subapps alloc]init];
            su.strSubappId = [dicSubapp objectForKey:@"subappId"];
            su.strSubappName = [dicSubapp objectForKey:@"subappName"];
            
            [arrSubapp addObject:su];
        }
        return arrSubapp;
    }
    else
    {
        return nil;
    }
}

//获取根应用下的子应用详细信息
+ (NSMutableArray *)getSubappsDeailWithRep:(RequestParameter *)reqParameter
{
    NSDictionary *resultDic = [NetWorking sendRequestWithUrlJsonGETParameter:reqParameter];
    NSDictionary *dicData = [resultDic objectForKey:@"data"];
    if (![dicData isKindOfClass:[NSNull class]] && dicData.count > 0)
    {
        NSArray *arrItems = [dicData objectForKey:@"items"];
        NSMutableArray *arrSubapp = [[NSMutableArray alloc]init];
        for (int i = 0; i < arrItems.count; i++)
        {
            NSDictionary *dicSubapp = [arrItems objectAtIndex:i];
            Subapps *su = [[Subapps alloc]init];
            su.strSubappImageUrl = [dicSubapp objectForKey:@"subappImageUrl"];
            su.strDescription = [dicSubapp objectForKey:@"description"];
            su.strSubappThumbnail = [dicSubapp objectForKey:@"subappThumbnail"];
            su.strSubappName = [dicSubapp objectForKey:@"subappName"];
            su.strSubappId = [dicSubapp objectForKey:@"subappId"];
            [arrSubapp addObject:su];
        }
        return arrSubapp;
    }
    else
    {
        return nil;
    }
}

//获取视频资源详细信息
+ (NSMutableArray *)getDetailResourceInfomationRequestParameter:(RequestParameter *)reqParameter
{
    NSMutableArray *arrInfo = [[NSMutableArray alloc]init];
    NSDictionary *resultDic = [NetWorking sendRequestWithUrlJsonGETParameter:reqParameter];
    NSDictionary *dicData = [resultDic objectForKey:@"data"];
    if (![dicData isKindOfClass:[NSNull class]] && dicData.count > 0)
    {
        DetailInfo *detail = [[DetailInfo alloc]init];
        detail.strEpisodesCount = [dicData objectForKey:@"episodesCount"];
        detail.strThumbnail = [dicData objectForKey:@"thumbnail"];
        detail.strTitle = [dicData objectForKey: @"title"];
        detail.strCategoryId = [dicData objectForKey: @"categoryId"];
        detail.strResUrl = [dicData objectForKey:@"resAppUrl"];
        NSArray *arrEpisodes = [dicData objectForKey:@"episodes"];
        for (int j = 0; j < arrEpisodes.count; j++)
        {
            detail.series = [[SeriesInfo alloc]init];
            NSDictionary *dicDetail = [arrEpisodes objectAtIndex:j];
            detail.series.strSeriesTitle = [dicDetail objectForKey:@"title"];
            detail.series.strPlayUrl = [dicDetail objectForKey:@"playUrl"];
        }
        detail.strResId = [dicData objectForKey:@"id"];
        [arrInfo addObject:detail];
    }
    return arrInfo;
}

@end
