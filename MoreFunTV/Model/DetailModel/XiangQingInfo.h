//
//  XiangQingInfo.h
//  MoreFunTV
//
//  Created by 沈成才 on 15-1-21.
//  Copyright (c) 2015年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XiangQingInfo : NSObject

//详情网址的resid
@property(nonatomic,retain) NSString * strResId;
//影片的第几集-默认附值1
@property(nonatomic,retain) NSString * strIndex;
//影片的来源-默认附值空
@property(nonatomic,retain) NSString * strSource;

@end
