//
//  Client.h
//  MoreFunTV
//
//  Created by admin on 14-10-30.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Client : NSObject
@property (nonatomic,retain) NSString *strClientId;//客户端ID
@property (nonatomic,retain) NSString *strEncryptKey;//编码
@property (nonatomic,retain) NSString *strRootAppId;//根appId
@property (nonatomic,retain) NSString *strVersion;//版本号

@end
