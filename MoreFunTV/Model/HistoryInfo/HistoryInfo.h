//
//  HistoryInfo.h
//  MoreFunTV
//
//  Created by admin on 2014-10-28.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HistoryInfo : NSObject
@property(nonatomic,retain)NSString *strTitle;      // 影片名
@property(nonatomic,retain)NSString *strThumbnail;  // 图片
@property(nonatomic,retain)NSString *strCreatedOn;  //观看的时间
@property(nonatomic,retain)NSString *strContinue;   //继续播放
@property(nonatomic,retain )NSString *strUrl;       //收藏列表的url
@property(nonatomic,retain )NSString *strId;

@end
