//
//  SetUpSingleton.m
//  MoreFunTV
//
//  Created by admin on 14-10-30.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "SetUpSingleton.h"

//创建一个单例对象  初始化一次
static SetUpSingleton *setUp_;
//创建一个单例对象  初始化一次  保存和传递投射视频名称
static NSString *titleString_;

@implementation SetUpSingleton

//单例模式类方法 实现 返回一个单例对象
+(SetUpSingleton *)sharedSetUp
{
    if (setUp_ == nil)
    {
        setUp_ = [[SetUpSingleton alloc]init];
        setUp_.strApiURL = @"http://api.cube.morefun.tv";
        setUp_.strRootAppId = @"740";
        setUp_.firstWeiShiID = YES;
        setUp_.firstDiFangID = YES;
        setUp_.firstWeiShiEpg = YES;
        setUp_.firstDiFangEPG = YES;
        setUp_.pageWeiShiNumber = 2;
        setUp_.pageDiFangNumber = 1;
        setUp_.strNetFlag = @"YES";
        setUp_.strHistoryFlag = @"YES";
        setUp_.strPlayFlag = @"YES";
        setUp_.strInfoFlag = @"YES";
        setUp_.strHidden = @"YES";
    }
    return setUp_;
}

@end
