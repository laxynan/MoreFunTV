//
//  SetUpSingleton.h
//  MoreFunTV
//
//  Created by admin on 14-10-30.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DetailInfo.h"

@interface SetUpSingleton : NSObject
@property(nonatomic,retain) NSString *strApiURL;////网络接口地址
@property (nonatomic,retain) NSString *strRootAppId;//根appId
@property (nonatomic,retain) NSString *strSubappId;//子应用id
@property (nonatomic,assign) int pageNumber;//页码
@property (nonatomic,assign) int pageWeiShiNumber;//卫视台信息总页码
@property (nonatomic,assign) int pageDiFangNumber;//地方台信息总页码
@property (nonatomic,assign) BOOL firstWeiShiID;//第一次请求卫视台数据ID
@property (nonatomic,assign) BOOL firstDiFangID;//第一次请求地方台数据ID
@property (nonatomic,assign) BOOL firstWeiShiEpg;//第一次请求卫视台数据EPG
@property (nonatomic,assign) BOOL firstDiFangEPG;//第一次请求地方台数据EPG

@property(nonatomic,retain) NSString *strNetFlag;//启用移动网络
@property(nonatomic,retain) NSString *strHistoryFlag;//存历史记录的标记
@property(nonatomic,retain) NSString *strInfoFlag;//消息通知的标记
@property(nonatomic,retain) NSString *strPlayFlag;//存播放记录的标记
@property(nonatomic,retain) NSString *strHidden;//按钮显示与否
@property(nonatomic,retain) DetailInfo *detail;//投射的对象
@property(nonatomic,retain) NSString *strTouSheURL;//投射的网址
@property(nonatomic,retain) NSString *strAllTouSheURL;//整集的网址

@property(nonatomic,assign) NSString *_titleString;//目前正在播放的视频标题

@property(nonatomic,retain) NSString *strAddTouShe;//显示添加队列
@property(nonatomic,assign) int count;//目前正在播放的视频
@property(nonatomic,assign) int arrCount;
@property(nonatomic,assign) BOOL isisConnected;

@property (nonatomic,retain) NSMutableArray *arrSheet;

//单例模式类方法
+(SetUpSingleton *)sharedSetUp;

@end
