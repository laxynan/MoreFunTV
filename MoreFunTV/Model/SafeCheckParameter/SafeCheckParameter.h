//
//  SafeCheckParameter.h
//  MoreFunTV
//
//  Created by admin on 14-10-25.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import "SetUpSingleton.h"

@interface SafeCheckParameter : NSObject
//安全校验参数拼接
+(NSString *)safeCheckParameterAppending;
//获取当前的事件戳
+(NSString *)timeSpCalculate;
@end
