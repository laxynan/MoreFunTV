//
//  AlertCustomView.h
//  CustomAlter
//
//  Created by admin on 14-12-11.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AlertCustomView;
@protocol AlertCustomViewDelegate <NSObject>
//投射按钮协议事件
-(void)customTouSheBtnAlterView:(AlertCustomView *)alter;

//网页播放和添加列表按钮协议事件
-(void)customWebORAddBtnAlterView:(AlertCustomView *)alter;

//cell的点击协议
-(void)cellTouchAlterView:(AlertCustomView *)alter RowAtIndexPath:(NSIndexPath *)indexPath;

//点击事件的协议
-(void)tapRecognizerAlterView:(AlertCustomView *)alterView;

@end

@interface AlertCustomView : UIView<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
{
    UITableView *_tableView;
    UIButton *_btnLeft;//左边按钮
    UIButton *_btnRight;//右边按钮
    BOOL _touShe;
}
@property(nonatomic,assign) BOOL isConnect;
@property(nonatomic,retain) NSMutableArray *arrDevice;//设备列表
@property(nonatomic,retain) NSMutableArray *arrCustom;
@property(nonatomic,assign)id<AlertCustomViewDelegate> delegate;

//右边按钮的图标变换
-(void)rightBtnImageChange;

//列表变换
-(void)tableChange;
@end
