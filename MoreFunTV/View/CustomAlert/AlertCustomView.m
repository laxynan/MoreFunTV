//
//  AlertCustomView.m
//  CustomAlter
//
//  Created by admin on 14-12-11.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "AlertCustomView.h"

@implementation AlertCustomView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        //创建点击手势对象
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
        tapRecognizer.delegate = self;
        //设置点击次数
        tapRecognizer.numberOfTapsRequired = 1;
        //关键语句，给self.view添加一个点击手势监测；
        [self addGestureRecognizer:tapRecognizer];
        
        //头部View
        UIView *viewBtn = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth-15*2, 100)];
        viewBtn.clipsToBounds = YES;
        viewBtn.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
        
        //左边按钮
        _btnLeft = [[UIButton alloc]initWithFrame:CGRectMake((kScreenWidth-2*15-2*90)/3.0, 5, 90, 90)];
        [_btnLeft setTitle:MyLocalizedString(@"custom1") forState:UIControlStateNormal];
        _btnLeft.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        [_btnLeft setImage:[UIImage imageNamed:@"playtoushe.png"] forState:UIControlStateNormal];
        [_btnLeft setImageEdgeInsets:UIEdgeInsetsMake(-30, 15, 0, 0)];//图片位置偏移
        [_btnLeft setTitleEdgeInsets:UIEdgeInsetsMake(40, -60, -12, 0)];//Title位置偏移
        _btnLeft.backgroundColor = [UIColor colorWithRed:247.0/255.0  green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
        [_btnLeft setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_btnLeft addTarget:self action:@selector(clickLeft) forControlEvents:UIControlEventTouchUpInside];
        [viewBtn addSubview:_btnLeft];
        
        //右边按钮
        _btnRight = [[UIButton alloc]initWithFrame:CGRectMake((kScreenWidth-2*15-2*90)/3.0*2.0+90, 5, 90, 90)];
        [_btnRight setTitle:MyLocalizedString(@"alertinfob") forState:UIControlStateNormal];
        _btnRight.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        [_btnRight setImage:[UIImage imageNamed:@"playliebiao.png"] forState:UIControlStateNormal];
        [_btnRight setImageEdgeInsets:UIEdgeInsetsMake(-30, 23, 5, 0)];//图片位置偏移
        [_btnRight setTitleEdgeInsets:UIEdgeInsetsMake(40, -25, -10, 0)];//Title位置偏移
        _btnRight.backgroundColor = [UIColor colorWithRed:247.0/255.0  green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
        [_btnRight setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_btnRight addTarget:self action:@selector(clickRight) forControlEvents:UIControlEventTouchUpInside];
        [viewBtn addSubview:_btnRight];

        
        //设备列表
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(15, (kScreenHeight-100)/2.0, kScreenWidth-15*2, 100) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = YES;//隐藏纵向滚动条
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.bounces = NO;
        _tableView.tableHeaderView = viewBtn;
        _tableView.layer.cornerRadius = 5.0;
        _tableView.clipsToBounds = YES;
        [_tableView setBackgroundColor:[UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000]];
        [self addSubview:_tableView];
        
        self.arrDevice = [[NSMutableArray alloc]init];
    }
    return self;
}

//列表每行显示的内容
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"TableAlert";
    //row 代表列表的那一行
    int row = (int)[indexPath row];
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle: UITableViewCellStyleDefault reuseIdentifier:identifier];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIView *viewLine = [[UIView alloc]initWithFrame:CGRectMake(15, 0.0, tableView.frame.size.width-15*2.0, 0.5)];
    viewLine.backgroundColor = [UIColor blackColor];
    [cell addSubview:viewLine];
    
    cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
    cell.textLabel.text = [self.arrDevice objectAtIndex:row];
    return cell;
}

//右边按钮的图标变换
-(void)rightBtnImageChange{
    if (self.isConnect == YES) {
        [_btnRight setTitle:MyLocalizedString(@"custom2") forState:UIControlStateNormal];
        [_btnRight setImage:[UIImage imageNamed:@"playtable.png"] forState:UIControlStateNormal];
        [_btnRight setImageEdgeInsets:UIEdgeInsetsMake(-30, 15, 0, 0)];//图片位置偏移
        [_btnRight setTitleEdgeInsets:UIEdgeInsetsMake(40, -47, -10, 0)];//Title位置偏移
    }
    else{
        [_btnRight setTitle:MyLocalizedString(@"alertinfob") forState:UIControlStateNormal];
        [_btnRight setImage:[UIImage imageNamed:@"playliebiao.png"] forState:UIControlStateNormal];
        [_btnRight setImageEdgeInsets:UIEdgeInsetsMake(-30, 23, 5, 0)];//图片位置偏移
        [_btnRight setTitleEdgeInsets:UIEdgeInsetsMake(40, -25, -10, 0)];//Title位置偏移
    }
}

//右边按钮的图标变换
-(void)tableChange{
    _touShe = NO;
    [UIView animateWithDuration:0.25f animations:^{
        self.arrDevice = nil;
        _tableView.frame = CGRectMake(15, (kScreenHeight-100)/2.0, kScreenWidth-15*2, 100);
        [_tableView reloadData];
    }];
}

//左边按钮
-(void)clickLeft{
    if (self.isConnect == YES) {
        if ([self.delegate respondsToSelector:@selector(customTouSheBtnAlterView:)]) {
            [self.delegate customTouSheBtnAlterView:self];
        }
    }
    else{
        if (_touShe == NO) {
            _touShe = YES;
            [UIView animateWithDuration:0.25f animations:^{
                self.arrDevice = self.arrCustom;
                _tableView.frame = CGRectMake(15, (kScreenHeight-100-self.arrDevice.count*45)/2.0, kScreenWidth-15*2, 100 + self.arrDevice.count*45);
                [_tableView reloadData];
            }];
        }
        else{
            _touShe = NO;
            [UIView animateWithDuration:0.25f animations:^{
                self.arrDevice = nil;
                _tableView.frame = CGRectMake(15, (kScreenHeight-100)/2.0, kScreenWidth-15*2, 100);
                [_tableView reloadData];
            }];
        }
    }
}

//右边按钮
-(void)clickRight{
    if ([self.delegate respondsToSelector:@selector(customWebORAddBtnAlterView:)]) {
        [self.delegate customWebORAddBtnAlterView:self];
    }
}


//cell点击事件
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [UIView animateWithDuration:0.25f animations:^{
        self.arrDevice = nil;
        _tableView.frame = CGRectMake(15, (kScreenHeight-100)/2.0, kScreenWidth-15*2, 100);
        [_tableView reloadData];
    }];
    
    if ([self.delegate respondsToSelector:@selector(cellTouchAlterView:RowAtIndexPath:)]) {
        [self.delegate cellTouchAlterView:self RowAtIndexPath:indexPath];
    }
}

//行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrDevice.count;
}

//列表每行的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

//点击手势点击事件
-(void)tapClick:(UITapGestureRecognizer *)sender
{
    if ([self.delegate respondsToSelector:@selector(tapRecognizerAlterView:)]) {
        [self.delegate tapRecognizerAlterView:self];
    }
}

//区分点击
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    // 输出点击的view的类名
//    NSLog(@"......%@", NSStringFromClass([touch.view class]));
    // 若为UITableViewCellContentView（即点击了tableViewCell），则不截获Touch事件
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableView"] || [NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"] || [NSStringFromClass([touch.view class]) isEqualToString:@"UIView"]) {
        return NO;
    }
    return  YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
