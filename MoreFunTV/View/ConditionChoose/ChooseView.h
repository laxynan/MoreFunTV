//
//  ChooseView.h
//  MoreFunTV
//
//  Created by admin on 14-10-22.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoInfo.h"

@class ChooseView;
@protocol ChooseViewDelegate <NSObject>

//按钮列表事件的协议方法
-(void)chooseView:(ChooseView *)chooseView Condition:(VideoInfo *)condition Button:(UIButton *)btn;

//点击事件的协议
-(void)tapRecognizerChooseView:(ChooseView *)chooseView Flag:(BOOL)flag Button:(UIButton *)btn;

@end

@interface ChooseView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableVideo;//条件列表
}
@property(nonatomic,assign) id<ChooseViewDelegate> delegate;
@property(nonatomic,retain) NSMutableArray *arrCondition;
@property(nonatomic,retain) UIButton *btn;
- (id)initWithCondition:(NSMutableArray *)arrCondition Button:(UIButton *)btn;
@end
