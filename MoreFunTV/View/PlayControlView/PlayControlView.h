//
//  PlayControlView.h
//  MoreFunTV
//
//  Created by admin on 14-11-3.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PlayControlView;
@protocol PlayControlViewDelegate <NSObject>

-(void)playControlView:(PlayControlView *)playView ButtonControl:(UIButton *)btn;

@end
@interface PlayControlView : UIView
@property(nonatomic,retain) UIButton *btnPlay;
@property(nonatomic,assign) id<PlayControlViewDelegate> delegate;

-(void)changBtnImge:(BOOL)flag;
@end
