//
//  VideoInfoView.m
//  MoreFunTV
//
//  Created by admin on 14-10-23.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "VideoInfoView.h"
#import "UIImageView+AFNetworking.h"

@implementation VideoInfoView

//初始化视频信息列表
- (id)initWithArrVideo:(NSMutableArray *)arrVideo
{
    self = [super init];
    if (self) {
        self.arrVideo = arrVideo;
        
        if (self.arrVideo.count%3 > 0)
        {
            _bCount = (int)self.arrVideo.count/3+1;
        }
        else
        {
            _bCount = (int)self.arrVideo.count/3;
        }
        
        for (int i = 0; i < _bCount; i++)
        {
            if (i == _bCount-1 && (int)self.arrVideo.count%3 > 0)
            {
                _aCount = (int)self.arrVideo.count%3;
            }
            else
            {
                _aCount = 3;
            }
            
            //影片信息按钮
            for (int j = 0; j < _aCount; j++)
            {
                //更改按钮数量。。在此处获取接口数据为按钮赋值
                VideoInfo *vInfo = [self.arrVideo objectAtIndex:_flag];
                //影片信息按钮
                UIButton *btnFilm = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                btnFilm.frame = CGRectMake((kScreenWidth-3*93)/4*(j+1)+93*j, 10+167*i, 93, 127);
                btnFilm.layer.borderWidth = 1.0;
                btnFilm.tag = 122 + _flag;
                _flag++;
                btnFilm.layer.borderColor = [[UIColor clearColor]CGColor];
                [btnFilm setBackgroundColor:[UIColor clearColor]];
                //添加点击事件
                [btnFilm addTarget:self action:@selector(clickFilm:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:btnFilm];
                
                //宣传图片
                UIImageView *imgFilm = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 93, 127)];
                
                //不是空的字符串
                if (![vInfo.strThumbnail isKindOfClass:[NSNull class]] && vInfo.strThumbnail.length > 0)
                {
                    //调试框架
                    [imgFilm setImageWithURL:[NSURL URLWithString:vInfo.strThumbnail] placeholderImage:[UIImage imageNamed:@"poster_default.png"]];
                }
                //空串
                else
                {
                    [imgFilm setImage:[UIImage imageNamed:@"poster_default.png"]];
                }
                imgFilm.backgroundColor = [UIColor lightGrayColor];
                [btnFilm addSubview:imgFilm];
                
                if ([vInfo.strCategoryId intValue] != 1) {
                    //影片概要
                    UILabel *labDescribe = [[UILabel alloc]initWithFrame:CGRectMake(0, 107, 93, 20)];
                    labDescribe.backgroundColor = [UIColor colorWithWhite:0.216 alpha:0.900];
                    labDescribe.font = [UIFont systemFontOfSize:12.0f];
                    labDescribe.textColor = [UIColor whiteColor];
                    labDescribe.textAlignment = NSTextAlignmentLeft;
                    
                    if ([vInfo.strReleasedEpisodes intValue] == 0) {
                        labDescribe.text = [NSString stringWithFormat:@"更新至最新"];
                    }
                    else{
                        labDescribe.text = [NSString stringWithFormat:@"更新至%@集",vInfo.strReleasedEpisodes];
                    }
                    [btnFilm addSubview:labDescribe];
                }
                
                //影片名称
                UILabel *labName = [[UILabel alloc]initWithFrame:CGRectMake(0, 127, 93, 30)];
                labName.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
                labName.font = [UIFont systemFontOfSize:15.0f];
                labName.textColor = [UIColor blackColor];
                labName.textAlignment = NSTextAlignmentCenter;
                labName.text = vInfo.strTitle;
                [btnFilm addSubview:labName];
            }
        }
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

//影片信息按钮点击方法
-(void)clickFilm:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(videoInfo:VideoInfo:)])
    {
        VideoInfo *info = [self.arrVideo objectAtIndex:sender.tag-122];
        [self.delegate videoInfo:self VideoInfo:info];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
