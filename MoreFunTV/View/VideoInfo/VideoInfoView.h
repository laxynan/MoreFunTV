//
//  VideoInfoView.h
//  MoreFunTV
//
//  Created by admin on 14-10-23.
//  Copyright (c) 2014年 admin. All rights reserved.
//
//  自定义视频信息列表
//

#import <UIKit/UIKit.h>
#import "VideoInfo.h"

@class VideoInfoView;
@protocol  VideoInfoViewDelegate <NSObject>

-(void)videoInfo:(VideoInfoView *)videoView VideoInfo:(VideoInfo *)videoInfo;
@end

@interface VideoInfoView : UIView
{
    //每行的个数
    int _aCount;
    //行数
    int _bCount;
    int _flag;
}
@property(nonatomic,assign) id<VideoInfoViewDelegate> delegate;
//影片信息按钮
//@property(nonatomic,retain) UIButton *btnFilm;
//影片宣传图
//@property(nonatomic,retain) UIImageView *imgFilm;
//影片概述
//@property(nonatomic,retain) UILabel *labDescribe;
//影片名称
//@property(nonatomic,retain) UILabel *labName;
//视频信息数组
@property(nonatomic,retain) NSMutableArray *arrVideo;
//初始化视频信息列表
- (id)initWithArrVideo:(NSMutableArray *)arrVideo;

@end
