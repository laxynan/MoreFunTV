

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface Loading : NSObject

+(void)showLoadingWithView:(UIView *)aView;

+(void)hiddonLoadingWithView:(UIView *)aView;

//投射控制页专用loading
+(void)showLoadingTouShe:(UIView *)aView;
@end
