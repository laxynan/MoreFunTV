//
//  Loading.m
//  图片加载第三方库
//
//  Created by 智享 on 14-8-16.
//  Copyright (c) 2014年 智享. All rights reserved.
//

#import "Loading.h"

@implementation Loading

+(void)showLoadingWithView:(UIView *)aView
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:aView animated:YES];
    hud.labelText = MyLocalizedString(@"loading");
//    //字体颜色
//    hud.labelColor = [UIColor blueColor];
//    //背景颜色
//    hud.color = [UIColor redColor];
}

+(void)hiddonLoadingWithView:(UIView *)aView
{
    //隐藏菊花
    [MBProgressHUD hideAllHUDsForView:aView animated:YES];
}

+(void)showLoadingTouShe:(UIView *)aView{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:aView animated:YES];
    hud.color = [UIColor clearColor];
}
@end
