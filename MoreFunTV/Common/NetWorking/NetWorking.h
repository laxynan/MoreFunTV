//
//  NetWorking.h
//  MoreFunTV
//
//  Created by admin on 14-10-25.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppendingURL.h"
#import "ASIFormDataRequest.h"
#import "ASIDownloadCache.h"

@interface NetWorking : NSObject
//发送GET请求请求接口数据
+ (NSDictionary *)sendRequestWithUrlJsonGETParameter:(RequestParameter *)reqParameter;

//发送GET请求请求分类信息
+ (NSMutableArray *)sendRequestClassInfoWithTypeName:(NSString *)typeName;

//请求联想搜索接口信息
+ (NSDictionary *)sendSearchWithURL:(NSString *)words;
@end
