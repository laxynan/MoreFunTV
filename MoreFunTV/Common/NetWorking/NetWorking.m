//
//  NetWorking.m
//  MoreFunTV
//
//  Created by admin on 14-10-25.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "NetWorking.h"
#import "AppDelegate.h"

@implementation NetWorking
//发送GET请求请求接口数据
+ (NSDictionary *)sendRequestWithUrlJsonGETParameter:(RequestParameter *)reqParameter;
{
    //请求的网址
    NSString *strUrl = @"";
    if ([reqParameter.strKeyWord isEqual:@"subapps"] || [reqParameter.strKeyWord isEqual:@"clients"]){
        strUrl = [AppendingURL strAppendingClientURLWith:reqParameter];
    }
    else{
        strUrl = [AppendingURL strAppendingURLWith:reqParameter];
    }
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//    NSLog(@"网址——————%@",url);
    //发送一个同步请求
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.requestMethod = @"GET";
    request.timeOutSeconds = 15;
    
    //获取全局变量
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //设置缓存方式
    [request setDownloadCache:appDelegate.myCache];
    //设置缓存数据存储策略，这里采取的是如果无更新或无法联网就读取缓存数据
    [request setCacheStoragePolicy:ASICachePermanentlyCacheStoragePolicy];
    request.delegate = self;
    [request startSynchronous];
    
    //如果请求成功返回数据字典
    if (request.error == nil){
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:request.responseData options:NSJSONReadingMutableContainers error:nil];
        return dic;
    }
    else{
        return nil;
    }
}

//发送GET请求请求分类信息
+ (NSMutableArray *)sendRequestClassInfoWithTypeName:(NSString *)typeName
{
    //请求的网址
    NSString *strUrl = [NSString stringWithFormat:classURL,typeName];
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//    NSLog(@"网址——————%@",url);
    //发送一个同步请求
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.requestMethod = @"GET";
    request.timeOutSeconds = 15;
    
    //获取全局变量
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //设置缓存方式
    [request setDownloadCache:appDelegate.myCache];
    //设置缓存数据存储策略，这里采取的是如果无更新或无法联网就读取缓存数据
    [request setCacheStoragePolicy:ASICachePermanentlyCacheStoragePolicy];
    request.delegate = self;
    [request startSynchronous];
    
    //如果请求成功返回数据字典
    if (request.error == nil){
        NSMutableArray *arr = [NSJSONSerialization JSONObjectWithData:request.responseData options:NSJSONReadingMutableContainers error:nil];
        return arr;
    }
    else{
        return nil;
    }
}

//请求联想搜索接口信息
+ (NSDictionary *)sendSearchWithURL:(NSString *)words
{
    //请求的网址
    NSString *strUrl = [NSString stringWithFormat:searchLx,words];
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    //    NSLog(@"网址——————%@",url);
    //发送一个同步请求
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.requestMethod = @"GET";
    request.timeOutSeconds = 15;
    
    //获取全局变量
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //设置缓存方式
    [request setDownloadCache:appDelegate.myCache];
    //设置缓存数据存储策略，这里采取的是如果无更新或无法联网就读取缓存数据
    [request setCacheStoragePolicy:ASICachePermanentlyCacheStoragePolicy];
    request.delegate = self;
    [request startSynchronous];
    
    //如果请求成功返回数据字典
    if (request.error == nil)
    {
        NSString *strData = [[NSString alloc]initWithData:request.responseData encoding:NSUTF8StringEncoding];
        if (strData.length == 0) {
            return nil;
        }
        else{
            NSString *strOne = [strData substringFromIndex:17];
            NSString *strResult =[strOne stringByReplacingOccurrencesOfString:@")" withString:@""];
            
            NSData *data = [strResult dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            return dic;
        }
    }
    else
    {
        return nil;
    }
}
@end
