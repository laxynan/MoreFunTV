//
//  DetailViewController.h
//  MoreFunTV
//
//  Created by admin on 2014-10-22.
//  Copyright (c) 2014Âπ? admin. All rights reserved.
//
//  详情页面
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "NetWorkingHelper.h"
#import "WebviewViewController.h"
#import "Loading.h"
#import "MatchstickDeviceController.h"
#import "RequestParameter.h"
#import "MatchStickViewController.h"
#import "DBDaoHelper.h"
#import "AlertCustomView.h"

@interface DetailViewController : BaseViewController<UIWebViewDelegate,UIWebViewDelegate,MatchstickControllerDelegate,UIAlertViewDelegate,AlertCustomViewDelegate>
{
    UIWebView *_webView;
    
    NSMutableArray *_arrDetail;
    UIWebView *_myWebView;
    RequestParameter *_rep;
    //定义播放前变量
    DetailInfo *_mediaToPlay0;
    //定义标题变量－暂存
    NSString *_strSubTitle;
    //定义集数变量－暂存
    NSString *_strSubIndex;
    //定义来源变量－暂存
    NSString *_strSubSource;
    BOOL _stop;
    BOOL _play;
    NSURL *_url;
    NSString * _strUrlWeb;//网页播放视频
    NSMutableArray *_arrSheet;
    NSString *_strRealUrl;
    NSString *_strRealUrlAll;
    NSDictionary *_urlDic;
    NSArray *_arr1;
    NSArray *_arr2;
    NSArray *_arr3;
    NSArray *_arr4;
    NSInteger RT_CODE;
    BOOL _push;
    AlertCustomView *_cusAlterView;
    BOOL _webPlay;
}
@property(nonatomic,retain) NSString *strResId;
//标题
@property(nonatomic,retain) NSString *strTitle;
//集数
@property(nonatomic,retain) NSString *strIndex;
//来源
@property(nonatomic,retain) NSString *strSource;

- (id)initWithResId:(NSString *)strResId;
@end
