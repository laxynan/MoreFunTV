//
//  DetailViewController.m
//  MoreFunTV
//
//  Created by admin on 2014-10-22.
//  Copyright (c) 2014????? admin. All rights reserved.
//

#import "DetailViewController.h"
#import "AppDelegate.h"
#import <Matchstick/Flint.h>
#import "static.h"
#import "lib/include/libanalysis/libanalysis.h"
#import "XiangQingInfo.h"

libanalysis *liba = [[libanalysis alloc]init];

@interface DetailViewController (){
    int lastKnownPlaybackTime;
    __weak MatchstickDeviceController *_matchstickDeviceController;
}
@property MPMoviePlayerController *moviePlayer;
@end

@implementation DetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithResId:(NSString *)strResId
{
    self = [super init];
    if (self) {
        _rep = [[RequestParameter alloc]init];
        _rep.portID = 7;
        _rep.strResId = strResId;
        _rep.strPageNo = @"1";
        _rep.strPageSize = @"30";
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            _arrDetail = [NetWorkingHelper getDetailResourceInfomationRequestParameter:_rep];
            dispatch_async(dispatch_get_main_queue(), ^{
                for (int i = 0; i < _arrDetail.count; i++) {
                    _mediaToPlay0 = [_arrDetail objectAtIndex:i];
                }
            });
        });
    }
    return self;
}

-(void)btnBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    _matchstickDeviceController = delegate.matchstickDeviceController;
    //投射图标判断
    if (_matchstickDeviceController.deviceScanner.devices.count > 0) {
        self.navigationItem.rightBarButtonItem = _matchstickDeviceController.matchstickBarButton;
    }
    
    //添加title
    [self addTitleNav];
    
    //添加WebView
    [self addWebView];
    
    _stop = YES;
    XiangQingInfo *info = [DBDaoHelper selectStrResId:[NSString stringWithFormat:@"%@",_rep.strResId]];
    //存在播放记录
    if (info.strIndex && ![info.strIndex isEqualToString:@""]) {
        _strIndex = info.strIndex;
        _strSource = info.strSource;
    }
    NSString *detailUrl = [NSString stringWithFormat:@"http://182.92.182.60/media/movie.html?resid=%@&security_code=%@&timestamp=%@&user_type=1&user_id=0&session_id=dltianwei&client_id=500&type=ios&index=%@&appid=%@",_rep.strResId,[SafeCheckParameter safeCheckParameterAppending],[SafeCheckParameter timeSpCalculate],_strIndex,_strSource];//@"42"
    _url = [NSURL URLWithString:detailUrl];
    NSLog(@"_myWebView_______url:%@",detailUrl);
    NSURLRequest *request=[[NSURLRequest alloc] initWithURL:_url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
    [_myWebView loadRequest:request];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _matchstickDeviceController.delegate = self;
}

- (void)shouldDisplayModalDeviceController {
    //投射按钮调用的方法
    [[NSNotificationCenter defaultCenter] postNotificationName:@"myDevice" object:nil userInfo:nil];
}

- (void)didDiscoverDeviceOnNetwork {
    // Add the Matchstick icon if not present.
    self.navigationItem.rightBarButtonItem = _matchstickDeviceController.matchstickBarButton;
}

//添加title
-(void)addTitleNav{
    self.navigationItem.titleView = nil;
    UIButton *btnBack = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
    [btnBack setTitle:MyLocalizedString(@"detail") forState:UIControlStateNormal];
    btnBack.titleLabel.font = [UIFont boldSystemFontOfSize:19.0f];
    [btnBack setBackgroundColor:[UIColor clearColor]];
    [btnBack setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBack) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"return.png"]];
    image.frame = CGRectMake(0, 12, 10, 20);
    [btnBack addSubview:image];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    _cusAlterView = [[AlertCustomView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    _cusAlterView.delegate = self;
    _cusAlterView.backgroundColor = [UIColor colorWithWhite:0.098 alpha:0.600];
    _cusAlterView.alpha = 0;
    [app.window addSubview:_cusAlterView];
}

//添加WebView
-(void)addWebView{
    
    UIView *viewA = [[UIView alloc]init];
    [self.view addSubview:viewA];
    _myWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight-64)];
    _myWebView.delegate = self;
    [(UIScrollView *)[[_myWebView subviews] objectAtIndex:0]setBounces:NO];
    [_myWebView setUserInteractionEnabled:YES];
    [self.view addSubview:_myWebView];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestString = request.mainDocumentURL.relativePath;
    if (_stop == NO){
        NSArray *arrWeb = [requestString componentsSeparatedByString:@"$"];
        if (arrWeb.count == 0) {
            _webPlay = YES;
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfo6") delegate:self cancelButtonTitle:MyLocalizedString(@"sure") otherButtonTitles:nil];
            [alert show];
        }
        else{
            NSString * str = [arrWeb objectAtIndex:1] ;
            //暂存标题
            _strSubTitle = [arrWeb objectAtIndex:2] ;
            //存在集数变量
            if (arrWeb.count > 3) {
                //暂存集数
                _strSubIndex = [arrWeb objectAtIndex:3] ;
            }
            else {
                //默认附值1
                _strSubIndex = @"1";
            }
            if (arrWeb.count > 0) {
                _strSubSource  = [[arrWeb firstObject] substringFromIndex:1];
            }
            else {
                //默认附值空
                _strSubSource = @"";
            }
            
//            NSLog(@"1------------------------------%@",_mediaToPlay0.strTitle);
            //网页播放的是视频地址存在
            if (![str isEqualToString:@""]){
                _strUrlWeb = str;
//                NSLog(@"_strUrlWebbo播放地址------------------------为:%@",_strUrlWeb);
                AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [Loading showLoadingWithView:app.window];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    //执行耗时操作
                    //解析数据
                    [self anlysisVoideInfo];
                    dispatch_async(dispatch_get_main_queue(), ^{//主线程更新UIí
//                        NSLog(@"_str解析到的地址------------------------为:%@",_strRealUrl);
                        [Loading hiddonLoadingWithView:app.window];
                        if (_strRealUrl.length == 0) {
                            _webPlay = NO;
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfoa") delegate:self cancelButtonTitle:MyLocalizedString(@"cancel") otherButtonTitles:MyLocalizedString(@"alertinfob"),nil];
                            [alert show];
                        }
                        else{
                            if (_matchstickDeviceController.deviceScanner.devices.count > 0) {
                                //调用设备选择页面
                                [self addAction];
                            }
                            else{
                                //网页播放
                                [self webPlay];
                            }
                        }
                    });
                });
                return NO;
            }
            else{
                _webPlay = YES;
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfo7") delegate:self cancelButtonTitle:MyLocalizedString(@"sure") otherButtonTitles:nil];
                [alert show];
            }
        }
        _stop = YES;
        return NO;
    }
    return true;
}

//解析数据
-(void)anlysisVoideInfo{
    //网址解析
    const LPCSTR url = [_strUrlWeb cStringUsingEncoding:NSASCIIStringEncoding];
    [liba URLTIsRunning];
    const int sourceId = 0;
    char realURL[1024*30];//= new char(1024*30);
    int len = 1024*30;
    const LPCSTR ua = "";
    const LPCSTR sourceKind = "";
    const LPCSTR spiderName = "";
    const LPCSTR videoParam = "";
//    NSLog(@"未解析前------------realURL----------------： %s",realURL);
    RT_CODE = [liba AnalysisRealUrlForAndroidMorefun: sourceId url:url realurl:realURL len:len ua:ua skind:sourceKind spiderName:spiderName videoParam:videoParam];
//    NSLog(@"返回值参数------------RT_CODE----------------： %d",RT_CODE);
    _strRealUrlAll = [NSString stringWithUTF8String:realURL];
//    NSLog(@"realURL解析到的内容------------realURL----------------： %s",realURL);
    if (_strRealUrlAll.length != 0){
//        NSLog(@"strRealUrlAll整集的播放地址----------------------------： %@",_strRealUrlAll);
        _urlDic = [NSJSONSerialization JSONObjectWithData: [_strRealUrlAll dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: nil];
        
        _arr1 = [_urlDic objectForKey:@"nd"];
        _arr2 = [_urlDic objectForKey:@"sd"];
        _arr3 = [_urlDic objectForKey:@"hd"];
        if(_arr1.count == 0){
            if (_arr2.count == 0) {
                if (_arr3.count != 0) {
                    _strRealUrl = [_arr3 objectAtIndex:0];
                }
            }else{
                _strRealUrl = [_arr2 objectAtIndex:0];
            }
        }
        else{
            _strRealUrl = [_arr1 objectAtIndex:0];
        }
    }
}

//调用设备选择页面
-(void)addAction{
    _arrSheet = [[NSMutableArray alloc]init];
    if (_matchstickDeviceController.isConnected) {
        _cusAlterView.isConnect = YES;
    }
    else{
        _cusAlterView.isConnect = NO;
        for (int i = 0; i < _matchstickDeviceController.deviceScanner.devices.count; i++) {
            MSFKDevice *device = [_matchstickDeviceController.deviceScanner.devices objectAtIndex:i];
            // only show LAN IP.
            if (device.ipAddress == nil
                || [device.ipAddress isEqualToString:@"192.168.1.1"]
                || !([device.ipAddress hasPrefix:@"192"]
                     || [device.ipAddress hasPrefix:@"172"]
                     || [device.ipAddress hasPrefix:@"10"])) {
                    //过滤掉的设备
                }
            else {
                [_arrSheet addObject:device.friendlyName];
            }
        }
        _cusAlterView.arrCustom = _arrSheet;
    }
    //图标变化
    [_cusAlterView rightBtnImageChange];
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView animateWithDuration:0.25f animations:^{
        _cusAlterView.alpha = 1.0;
    }];
}

//投射按钮协议事件
-(void)customTouSheBtnAlterView:(AlertCustomView *)alter{
    [self touPlay];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView animateWithDuration:0.25f animations:^{
        _cusAlterView.alpha = 0.0;
    }];
}

//网页播放和添加列表按钮协议事件
-(void)customWebORAddBtnAlterView:(AlertCustomView *)alter
{
    //网页播放
    if (_cusAlterView.isConnect == NO) {
        [self webPlay];
        [_cusAlterView tableChange];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView animateWithDuration:0.25f animations:^{
            _cusAlterView.alpha = 0.0;
        }];
    }
    //添加队列
    else{
        DetailInfo *_mediaToPlay1 = [[DetailInfo alloc] init];
        _mediaToPlay1 = _mediaToPlay0;
        _mediaToPlay1.strTitle = _strSubTitle;
        _mediaToPlay1.strIndex = _strSubIndex;
        _mediaToPlay1.strSource = _strSubSource;
        //标题包含视频名称
        if ([_mediaToPlay1.strTitle rangeOfString:self.strTitle].length > 0) {
//            dea.strTitle = _mediaToPlay1.strTitle;
        }
        else {
            _mediaToPlay1.strTitle = [self.strTitle stringByAppendingString:_mediaToPlay1.strTitle];
        }
        BOOL result = [DBDaoHelper selectTouShe:_mediaToPlay1.strTitle];
        if (result == YES) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfo8") delegate:nil cancelButtonTitle:MyLocalizedString(@"sure") otherButtonTitles:nil];
            [alert show];
        }
        else{
            SetUpSingleton *set = [SetUpSingleton sharedSetUp];
            set.strAddTouShe = @"YES";
            
            DetailInfo *_mediaToPlay1 = [[DetailInfo alloc] init];
            _mediaToPlay1 = _mediaToPlay0;
            _mediaToPlay1.strTitle = _strSubTitle;
            _mediaToPlay1.strIndex = _strSubIndex;
            _mediaToPlay1.strSource = _strSubSource;
            //获取当前时间
            NSDate *datenow = [[NSDate alloc]init];
            _mediaToPlay1.dateTime = datenow;
            _mediaToPlay1.strHistoryTime = @"";//0分0秒";
            _mediaToPlay1.strResUrl = _strUrlWeb;
            SetUpSingleton *setUp = [SetUpSingleton sharedSetUp];
            
            DetailInfo *detail0 = [[DetailInfo alloc]init];
            detail0 = _mediaToPlay1;
            //标题包含视频名称
            if ([_mediaToPlay1.strTitle rangeOfString:self.strTitle].length > 0) {
                
            }
            else {
                detail0.strTitle = [self.strTitle stringByAppendingString:_mediaToPlay1.strTitle];
            }
            if ([setUp.strPlayFlag isEqual:@"YES"]) {
                detail0.strPlaySource = @"addList";
                detail0.strTitleString = self.strTitle;
                //播放记录存入数据库
                [DBDaoHelper insertPlay:detail0];
            }
            //获取strPlayId
            NSString *strPlayId = [DBDaoHelper selectStrPlayId];
            
            
            DetailInfo *dea = [[DetailInfo alloc]init];
            //标题包含视频名称
            if ([_mediaToPlay1.strTitle rangeOfString:self.strTitle].length > 0) {
                dea.strTitle = _mediaToPlay1.strTitle;
            }
            else {
                dea.strTitle = [self.strTitle stringByAppendingString:_mediaToPlay1.strTitle];
            }
            dea.strPlayId = strPlayId;//@"0";
            dea.strRealUrl = _strRealUrl;
            dea.strAllURL = _strRealUrlAll;
            dea.strThumbnail = _mediaToPlay1.strThumbnail;
            //查询投射信息是否已经插入
            if (![DBDaoHelper selectTouShe:dea.strTitle]) {
                //插入投射记录信息
                [DBDaoHelper insertTouShe:dea];
            }
        }
        
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView animateWithDuration:0.25f animations:^{
            _cusAlterView.alpha = 0.0;
        }];
    }
}

//cell的点击协议
-(void)cellTouchAlterView:(AlertCustomView *)alter RowAtIndexPath:(NSIndexPath *)indexPath{
    SetUpSingleton *set = [SetUpSingleton sharedSetUp];
    NSMutableArray *deviceArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < _matchstickDeviceController.deviceScanner.devices.count; i++) {
        MSFKDevice *device = [_matchstickDeviceController.deviceScanner.devices objectAtIndex:i];
        // only show LAN IP.
        if (device.ipAddress == nil
            || [device.ipAddress isEqualToString:@"192.168.1.1"]
            || !([device.ipAddress hasPrefix:@"192"]
                 || [device.ipAddress hasPrefix:@"172"]
                 || [device.ipAddress hasPrefix:@"10"])) {
                //过滤掉的设备
            }
        else {
            [deviceArr addObject:device];
        }
    }
    MSFKDevice *device = [deviceArr objectAtIndex:indexPath.row];
    NSLog(@"Selecting device2:%@", device.friendlyName);
    [_matchstickDeviceController connectToDevice:device];
    
    NSString *str = [_arrSheet objectAtIndex:[indexPath row]];
    set.arrSheet = [[NSMutableArray alloc]init];
    [set.arrSheet addObject:str];
    
    _push = YES;
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView animateWithDuration:0.25f animations:^{
        _cusAlterView.alpha = 0.0;
    }];
}

//点击事件的协议
-(void)tapRecognizerAlterView:(AlertCustomView *)alterView{
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView animateWithDuration:0.25f animations:^{
        [_cusAlterView tableChange];
        _cusAlterView.alpha = 0.0;
    }];
}

//网页播放按钮的事件
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        if (_webPlay == YES) {
            _stop = YES;
            NSString *detailUrl = [NSString stringWithFormat:@"http://182.92.182.60/media/movie.html?resid=%@&security_code=%@&timestamp=%@&user_type=1&user_id=0&session_id=dltianwei&client_id=500&type=ios&index=%@&appid=%@",_rep.strResId,[SafeCheckParameter safeCheckParameterAppending],[SafeCheckParameter timeSpCalculate],_strIndex,_strSource];//@"42"
            _url = [NSURL URLWithString:detailUrl];
            NSURLRequest *request=[[NSURLRequest alloc] initWithURL:_url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
            [_myWebView loadRequest:request];
        }
    }
    else if (buttonIndex == 1){
        if (_webPlay == NO) {
            //网页播放
            [self webPlay];
        }
    }
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [Loading showLoadingWithView:self.view];
    if (_stop == YES) {
        _stop = NO;
    }
    else{
        _stop = YES;
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [Loading hiddonLoadingWithView:self.view];
}

- (void)didConnectToDevice:(MSFKDevice *)device {
    lastKnownPlaybackTime = [self.moviePlayer currentPlaybackTime];
    [self.moviePlayer stop];
    if (_push == YES) {
        _push = NO;
        [self touPlay];
    }
}

//投射播放
-(void)touPlay{
    SetUpSingleton *set = [SetUpSingleton sharedSetUp];
    //隐藏浮动的按钮
    set.strHidden = @"YES";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BtnSetHidden" object:nil userInfo:nil];
    
    UIButton *matchstickButton = (UIButton *) _matchstickDeviceController.matchstickBarButton.customView;
    [matchstickButton setImage:[UIImage imageNamed:@"playing.png"] forState:UIControlStateNormal];
//    NSLog(@"_____________playing.png");
    
    DetailInfo *_mediaToPlay1 = [[DetailInfo alloc] init];
    _mediaToPlay1 = _mediaToPlay0;
    _mediaToPlay1.strTitle = _strSubTitle;
    _mediaToPlay1.strIndex = _strSubIndex;
    _mediaToPlay1.strSource = _strSubSource;
    //获取当前时间
    NSDate *datenow = [[NSDate alloc]init];
    _mediaToPlay1.dateTime = datenow;
    _mediaToPlay1.strHistoryTime = @"";//0分0秒";
    _mediaToPlay1.strResUrl = _strUrlWeb;
    SetUpSingleton *setUp = [SetUpSingleton sharedSetUp];
    
    DetailInfo *detail0 = [[DetailInfo alloc]init];
    detail0 = _mediaToPlay1;
    //标题包含视频名称
    if ([_mediaToPlay1.strTitle rangeOfString:self.strTitle].length > 0) {
        
    }
    else {
        detail0.strTitle = [self.strTitle stringByAppendingString:_mediaToPlay1.strTitle];
    }
    if ([setUp.strPlayFlag isEqual:@"YES"]) {
        detail0.strPlaySource = @"touPlay";
        detail0.strTitleString = self.strTitle;
        //播放记录存入数据库
        [DBDaoHelper insertPlay:detail0];
    }
    
    XiangQingInfo *info = [[XiangQingInfo alloc] init];
    info = [DBDaoHelper selectStrResId:[NSString stringWithFormat:@"%@",_mediaToPlay1.strResId]];
    if (info.strIndex && ![info.strIndex isEqualToString:@""]) {
        info.strResId = [NSString stringWithFormat:@"%@",detail0.strResId];
        info.strIndex = detail0.strIndex;
        info.strSource = detail0.strSource;
        [DBDaoHelper updateXiangQing:info];
    }
    else {
        info.strResId = [NSString stringWithFormat:@"%@",detail0.strResId];
        info.strIndex = detail0.strIndex;
        info.strSource = detail0.strSource;
        [DBDaoHelper insertXiangQing:info];
    }
    
    //获取strPlayId
    NSString *strPlayId = [DBDaoHelper selectStrPlayId];
    
    set.strAddTouShe = @"NO";
    set.count = 0;
    DetailInfo *detail = [[DetailInfo alloc]init];
    //标题包含视频名称
    if ([_mediaToPlay1.strTitle rangeOfString:self.strTitle].length > 0) {
        detail.strTitle = _mediaToPlay1.strTitle;
    }
    else {
        detail.strTitle = [self.strTitle stringByAppendingString:_mediaToPlay1.strTitle];
    }
    detail.strRealUrl = _strRealUrl;
    detail.strAllURL = _strRealUrlAll;
    detail.strThumbnail = _mediaToPlay1.strThumbnail;
    //查询投射信息是否已经插入
    if (![DBDaoHelper selectTouShe:detail.strTitle]) {
        detail.strPlayId = strPlayId;
        //插入投射记录信息
        [DBDaoHelper insertTouShe:detail];
    }
    
    _mediaToPlay1 = [[DetailInfo alloc] init];
    _mediaToPlay1 = _mediaToPlay0;
    _mediaToPlay1.strTitle = _strSubTitle;
    _mediaToPlay1.strIndex = _strSubIndex;
    _mediaToPlay1.strSource = _strSubSource;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateHistoryPlay" object:nil userInfo:nil];
    //投射播放
    MatchStickViewController *mat = [[MatchStickViewController alloc]initWithRealUrl:_strRealUrl];
    DetailInfo *detail2 = [[DetailInfo alloc]init];
    detail2 = _mediaToPlay1;
    detail2.strPlayId = strPlayId;
    mat.mediaToPlay = detail2;
    //标题包含视频名称
    if ([_mediaToPlay1.strTitle rangeOfString:self.strTitle].length > 0) {
    }
    else {
        mat.mediaToPlay.strTitle = [self.strTitle stringByAppendingString:_mediaToPlay1.strTitle];
    }
    [SetUpSingleton sharedSetUp]._titleString = _mediaToPlay1.strTitle;
//    NSLog(@"_______________%@",[SetUpSingleton sharedSetUp]._titleString);
    mat.strAllRealUrl = _strRealUrlAll;
    [self presentViewController:mat animated:YES completion:^{
    }];
}

//网页播放
-(void)webPlay
{
    DetailInfo *_mediaToPlay1 = [[DetailInfo alloc] init];
    _mediaToPlay1 = _mediaToPlay0;
    _mediaToPlay1.strTitle = _strSubTitle;
    _mediaToPlay1.strIndex = _strSubIndex;
    _mediaToPlay1.strSource = _strSubSource;
    //获取当前时间
    NSDate *datenow = [[NSDate alloc]init];
    _mediaToPlay1.dateTime = datenow;
    _mediaToPlay1.strHistoryTime = @"";//0分0秒";
    _mediaToPlay1.strResUrl = _strUrlWeb;
    SetUpSingleton *setUp = [SetUpSingleton sharedSetUp];
    
    DetailInfo *detail0 = [[DetailInfo alloc]init];
    detail0 = _mediaToPlay1;
    //标题包含视频名称
    if ([_mediaToPlay1.strTitle rangeOfString:self.strTitle].length > 0) {
        
    }
    else {
        detail0.strTitle = [self.strTitle stringByAppendingString:_mediaToPlay1.strTitle];
    }
    if ([setUp.strPlayFlag isEqual:@"YES"]) {
        detail0.strPlaySource = @"webPlay";
        detail0.strTitleString = self.strTitle;
        //播放记录存入数据库
        [DBDaoHelper insertPlay:detail0];
    }
    
    XiangQingInfo *info = [[XiangQingInfo alloc] init];
    info = [DBDaoHelper selectStrResId:[NSString stringWithFormat:@"%@",_mediaToPlay1.strResId]];
    if (info.strIndex && ![info.strIndex isEqualToString:@""]) {
        info.strResId = [NSString stringWithFormat:@"%@",detail0.strResId];
        info.strIndex = detail0.strIndex;
        info.strSource = detail0.strSource;
        [DBDaoHelper updateXiangQing:info];
    }
    else {
        info.strResId = [NSString stringWithFormat:@"%@",detail0.strResId];
        info.strIndex = detail0.strIndex;
        info.strSource = detail0.strSource;
        [DBDaoHelper insertXiangQing:info];
    }
    
    //网页播放视频
    WebviewViewController *we = [[WebviewViewController alloc]initWithHtmlUrl:_strUrlWeb];
    [self presentViewController:we animated:YES completion:^{
    }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
