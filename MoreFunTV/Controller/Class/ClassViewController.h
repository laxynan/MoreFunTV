//
//  ClassViewController.h
//  MoreFunTV
//
//  Created by admin on 14-10-21.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ClassTableViewCell.h"
#import "VideoTableViewController.h"
#import "LiveTableViewController.h"
#import "NetWorkingHelper.h"
#import "Loading.h"
#import "AFNetworking.h"
#import "MatchstickDeviceController.h"
#import <Matchstick/Flint.h>

@interface ClassViewController : BaseViewController<MatchstickControllerDelegate>
{
    UIScrollView *_scrollViewClass;//分类UI
    NSMutableArray *_arrClass;//分类数组
    VideoInfo *_class;
    int _aCount;//行
    int _bCount;//列
    int _flag;
    
    UIImageView *_imgV;
    UIImage *_img;
}
@end
