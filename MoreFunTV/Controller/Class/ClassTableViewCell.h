//
//  ClassTableViewCell.h
//  MoreFunTV
//
//  Created by admin on 14-10-21.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassTableViewCell : UITableViewCell
@property(nonatomic,retain) UILabel *labClassName;//分类名称
@property(nonatomic,retain) UIView *viewLine;//分组下边线
@end
