//
//  WebviewViewController.m
//  MoreFunTV
//
//  Created by admin on 2014-10-29.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "UIWebView+VideoControl.h"
#import "WebviewViewController.h"

@interface WebviewViewController ()

@end

@implementation WebviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithHtmlUrl:(NSString *) strurl
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.strURL = strurl;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //添加title
    [self addTitleNav];
    
    UIView *aView = [[UIView alloc]init];
    [self.view addSubview:aView];
    
    _webView = [[UIWebView alloc] init];
    _webView.frame = CGRectMake(0, 64, kScreenWidth, kScreenHeight-64);
    _webView.backgroundColor = [UIColor whiteColor];
    _webView.delegate = self;
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:self.strURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
    [_webView loadRequest:request];
    [_webView setUserInteractionEnabled:YES];
    [(UIScrollView *)[[_webView subviews] objectAtIndex:0]setBounces:NO];//禁止webview越界滑动
    [self.view addSubview:_webView];

}

//添加title
-(void)addTitleNav{
    
    UIView *viewDiBu = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 64)];
    viewDiBu.backgroundColor = [UIColor colorWithWhite:0.280 alpha:1.000];
    [self.view addSubview:viewDiBu];
    
    UIButton *btnBack = [[UIButton alloc]initWithFrame:CGRectMake(15, 20, 80, 44)];
    [btnBack setTitle:@" 返回" forState:UIControlStateNormal];
    btnBack.titleLabel.font = [UIFont boldSystemFontOfSize:19.0f];
    [btnBack setBackgroundColor:[UIColor clearColor]];
    [btnBack setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBack) forControlEvents:UIControlEventTouchUpInside];
    [viewDiBu addSubview:btnBack];
    
    UIImageView *image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"return.png"]];
    image.frame = CGRectMake(0, 12, 10, 20);
    [btnBack addSubview:image];
}

-(void)btnBack{
    //视频信息
    NSLog(@"getVideoTitle:%@",[_webView getVideoTitle]);
    NSLog(@"getVideoDuration:%f",[_webView getVideoDuration]);
    NSLog(@"getVideoCurrentTime:%f",[_webView getVideoCurrentTime]);
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

//-(void)webViewDidStartLoad:(UIWebView *)webView
//{
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MoviePlayerLoadStateDidChange:)
//                                                 name:MPMoviePlayerLoadStateDidChangeNotification
//                                               object:nil];
//    [Loading showLoadingWithView:self.view];
//}
//
//
//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    [Loading hiddonLoadingWithView:self.view];
//}
//
//
//- (void) MoviePlayerLoadStateDidChange:(NSNotification*)notification {
//    MPMoviePlayerController *player = notification.object;
//    MPMovieLoadState loadState = player.loadState;
//    
//    if(loadState & MPMovieLoadStateUnknown){
//        NSLog(@"未知状态");
//    }
//    
//    if(loadState & MPMovieLoadStatePlayable){
//        //第一次加载，或者前后拖动完成之后
//        NSLog(@"可播放");
//    }
//    
//    if(loadState & MPMovieLoadStatePlaythroughOK){
//        NSLog(@"网络缓冲完成");
//    }
//    
//    if(loadState & MPMovieLoadStateStalled){
//        
//        NSLog(@"网络缓冲");
//    }
//}

#pragma mark webview每次加载之前都会调用这个方法
// 如果返回NO，代表不允许加载这个请求
//- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
//    // 说明协议头是ios
////    if ([@"ios" isEqualToString:request.URL.scheme]) {
////        NSString *url = request.URL.absoluteString;
////        NSRange range = [url rangeOfString:@":"];
////        NSString *method = [request.URL.absoluteString substringFromIndex:range.location + 1];
////        
////        SEL selector = NSSelectorFromString(method);
////        
////        if ([self respondsToSelector:selector]) {
////            [self performSelector:selector];
////        }
////        
////        return NO;
////    }
//    NSString *url = request.URL.absoluteString;
//    
//    return YES;
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
