//
//  ClientXMLViewController.h
//  MoreFunTV
//
//  Created by admin on 14-10-30.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClientXMLViewController : UIViewController<NSXMLParserDelegate>
{
    NSString *_strApiURL;
}
@property(nonatomic,retain) NSMutableString *strCurrent;
@property(nonatomic,retain) NSString *data;
@property(nonatomic,retain) NSMutableDictionary *twitterDic;

-(NSString *)ClientXMLAnalysis;
@end
