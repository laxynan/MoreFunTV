//
//  LiveTableViewCell.m
//  MoreFunTV
//
//  Created by admin on 14-10-24.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "LiveTableViewCell.h"

@implementation LiveTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        //节目图片
        self.img = [[UIImageView alloc]initWithFrame:CGRectMake(10, 16, 92, 66)];
        self.img.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
        self.img.layer.borderWidth = 1.1;
        self.img.layer.borderColor = [[UIColor colorWithWhite:0.422 alpha:1.000]CGColor];
        self.img.clipsToBounds = YES;
        [self addSubview:self.img];
        
        //电视台
        self.labTvName = [[UILabel alloc]initWithFrame:CGRectMake(10+92+15, 16, kScreenWidth-15-92-10-10, 22)];
        self.labTvName.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
        self.labTvName.font = [UIFont boldSystemFontOfSize:18.0f];
        self.labTvName.textColor = [UIColor colorWithWhite:0.075 alpha:1.000];
        self.labTvName.textAlignment = NSTextAlignmentLeft;
        [self addSubview:self.labTvName];
        
        //正在播放
        UILabel *labNowPlay = [[UILabel alloc]initWithFrame:CGRectMake(10+92+15, 16+22, 65, 22)];
        labNowPlay.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
        labNowPlay.font = [UIFont systemFontOfSize:14.0f];
        labNowPlay.textColor = [UIColor colorWithWhite:0.075 alpha:1.000];
        labNowPlay.text = @"正在直播:";
        labNowPlay.textAlignment = NSTextAlignmentLeft;
        [self addSubview:labNowPlay];
        
        //正在播放的剧场
        self.labNowTheatre = [[UILabel alloc]initWithFrame:CGRectMake(10+92+15+65, 16+22, kScreenWidth-10-92-15-65-15, 22)];
        self.labNowTheatre.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
        self.labNowTheatre.font = [UIFont systemFontOfSize:14.0f];
        self.labNowTheatre.textColor = [UIColor colorWithWhite:0.075 alpha:1.000];
        self.labNowTheatre.text = @"情感剧场:";
        self.labNowTheatre.textAlignment = NSTextAlignmentLeft;
        [self addSubview:self.labNowTheatre];
        
        //即将播放
        UILabel *labWillPlay = [[UILabel alloc]initWithFrame:CGRectMake(10+92+15, 16+22+22, 65, 22)];
        labWillPlay.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
        labWillPlay.font = [UIFont systemFontOfSize:14.0f];
        labWillPlay.textColor = [UIColor colorWithWhite:0.075 alpha:1.000];
        labWillPlay.text = @"即将播放:";
        labWillPlay.textAlignment = NSTextAlignmentLeft;
        [self addSubview:labWillPlay];
        
        //即将播放的剧场
        self.labWillTheatre = [[UILabel alloc]initWithFrame:CGRectMake(10+92+15+65, 16+22+22, kScreenWidth-10-92-15-65-15, 22)];
        self.labWillTheatre.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
        self.labWillTheatre.font = [UIFont systemFontOfSize:14.0f];
        self.labWillTheatre.textColor = [UIColor colorWithWhite:0.075 alpha:1.000];
        self.labWillTheatre.text = @"晚间剧场:";
        self.labWillTheatre.textAlignment = NSTextAlignmentLeft;
        [self addSubview:self.labWillTheatre];
        
        UIView *viewline = [[UIView alloc]initWithFrame:CGRectMake(0, 89.5, kScreenWidth, 0.5)];
        viewline.backgroundColor = [UIColor colorWithWhite:0.669 alpha:1.000];
        [self addSubview:viewline];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
