//
//  SearchViewController.h
//  MoreFunTV
//
//  Created by admin on 14-10-21.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ClassTableViewCell.h"
#import "NetWorkingHelper.h"
#import "Loading.h"
#import "DetailViewController.h"
#import "DBDaoHelper.h"
#import "ClassViewController.h"
#import "MatchstickDeviceController.h"
#import <Matchstick/Flint.h>

@interface SearchViewController : BaseViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,MatchstickControllerDelegate>
{
    UITableView    *_tableSearch;//搜索列表
    NSMutableArray *_arrSearch;//搜索列表数组
    UIView         *_searchBgView;
    UITextField    *_txtSearch;
    UIButton       *_btnSearch;//搜索按钮
    UIView         *_promptView;//提示的View
    UITableView    *_tableHot;//热门列表
    NSMutableArray *_arrHot;//热门列表数组
    NSMutableArray *_arrHistory;//历史记录列表
    NSMutableArray *_arrHHH;
    BOOL           _flag;
    BOOL           _flagDB;//是否存历史记录
    BOOL           _isChinese;//是否中午输入法
}
@end
