//
//  SearchTableViewCell.h
//  MoreFunTV
//
//  Created by admin on 14-11-22.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewCell : UITableViewCell
@property(nonatomic,retain) UIImageView *imageVoide;//视频图片
@property(nonatomic,retain) UILabel *labTitle;//名称
@property(nonatomic,retain) UILabel *labClass;//分类名
@property(nonatomic,retain) UILabel *labDescription;//描述
@end
