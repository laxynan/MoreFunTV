//
//  SearchTableViewCell.m
//  MoreFunTV
//
//  Created by admin on 14-11-22.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "SearchTableViewCell.h"

@implementation SearchTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //视频图片
        _imageVoide =[[UIImageView alloc]init];
        _imageVoide.frame = CGRectMake(15, 7, 80, 106);
        _imageVoide.backgroundColor = [UIColor grayColor];
        [self addSubview:_imageVoide];
        
        //视频标题
        _labTitle = [[UILabel alloc]init];
        _labTitle.frame = CGRectMake(105, 5, kScreenWidth-105-15, 30);
        _labTitle.backgroundColor = [UIColor clearColor];
        _labTitle.textAlignment = NSTextAlignmentLeft;
        _labTitle.font = [UIFont boldSystemFontOfSize:16.0f];
        [self addSubview:_labTitle];
        
        //分类
        UILabel *labCl = [[UILabel alloc]init];
        labCl.frame = CGRectMake(105, 35, 40, 20);
        labCl.backgroundColor = [UIColor clearColor];
        labCl.textColor = [UIColor colorWithWhite:0.567 alpha:1.000];
        labCl.textAlignment = NSTextAlignmentLeft;
        labCl.text = @"类型:";
        labCl.font = [UIFont systemFontOfSize:14.0f];
        [self addSubview:labCl];
        
        _labClass = [[UILabel alloc]init];
        _labClass.frame = CGRectMake(145, 35, kScreenWidth-145-15, 20);
        _labClass.textAlignment = NSTextAlignmentLeft;
        _labClass.font = [UIFont systemFontOfSize:14.0f];
        _labClass.textColor = [UIColor colorWithWhite:0.567 alpha:1.000];
        _labClass.backgroundColor = [UIColor clearColor];
        [self addSubview:_labClass];
        
        //描述
        UILabel *labDes = [[UILabel alloc]init];
        labDes.frame = CGRectMake(105, 58, 40, 20);
        labDes.backgroundColor = [UIColor clearColor];
        labDes.textAlignment = NSTextAlignmentLeft;
        labDes.textColor = [UIColor colorWithWhite:0.567 alpha:1.000];
        labDes.text = @"详情:";
        labDes.font = [UIFont systemFontOfSize:14.0f];
        [self addSubview:labDes];
        
        _labDescription = [[UILabel alloc]init];
        _labDescription.frame = CGRectMake(145, 55, kScreenWidth-145-15, 60);
        _labDescription.textAlignment = NSTextAlignmentLeft;
        _labDescription.numberOfLines = 3;
        _labDescription.font = [UIFont systemFontOfSize:14.0f];
        _labDescription.textColor = [UIColor colorWithWhite:0.567 alpha:1.000];
        _labDescription.backgroundColor = [UIColor clearColor];
        [self addSubview:_labDescription];
    }
    return self;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
