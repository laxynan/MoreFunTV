//
//  FilmModelViewController.h
//  MoreFunTV
//
//  Created by admin on 14-10-23.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Loading.h"
#import <SafariServices/SafariServices.h>
#import "BookMarksViewController.h"
#import "DBDaoHelper.h"
#import "Search.h"
#import "SetUpSingleton.h"

@interface FilmModelViewController : UIViewController<UIWebViewDelegate,UITextFieldDelegate,UISearchBarDelegate>
{
    UIView *_websView;
    UIButton *_btnSearch;//搜索按钮
    NSString *_strTxt;
    BOOL _flag;
    NSString *_strTextSearch;
    NSString *_strURL;
}
@property(nonatomic,retain) UIWebView *webView;
@property(nonatomic,retain) UIButton *btnNext;
@property(nonatomic,retain) UIButton *btnBack;
@property(nonatomic,retain) UIButton *btnHome;
@end
