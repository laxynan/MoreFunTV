//
//  BookMarksViewController.h
//  MoreFunTV
//
//  Created by admin on 2014-11-9.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "DBDaoHelper.h"
#import "BookMarksTableViewCell.h"
#import "MatchStickViewController.h"
#import "WebviewViewController.h"

@interface BookMarksViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    UILabel *_labTitle;//标题title
    UITableView *_tableView;//列表
    NSMutableArray *_arrCollect;//收藏数组
    NSMutableArray *_arrMark;//历史记录数组
    BOOL _isClick;
    NSString *_strTitle;//web标题
    NSString *_strUrl;//web网址
    UIButton *_btnAdd;//添加按钮
    UISegmentedControl *_seg;//选择控件
    int _isAlter;
}
-(id)initWithTitle:(NSString *)title strURL:(NSString *)strurl;
@end
