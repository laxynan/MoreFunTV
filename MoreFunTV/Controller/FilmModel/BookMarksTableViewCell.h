//
//  BookMarksTableViewCell.h
//  MoreFunTV
//
//  Created by admin on 14-11-11.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookMarksTableViewCell : UITableViewCell
@property(nonatomic,retain) UILabel *labTiTle;//标题
@property(nonatomic,retain) UILabel *labUrl;//网址
@end
