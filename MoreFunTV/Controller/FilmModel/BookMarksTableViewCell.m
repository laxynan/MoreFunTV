//
//  BookMarksTableViewCell.m
//  MoreFunTV
//
//  Created by admin on 14-11-11.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "BookMarksTableViewCell.h"

@implementation BookMarksTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        //标题
        self.labTiTle = [[UILabel alloc]init];
        self.labTiTle.frame = CGRectMake(15, 0, kScreenWidth-15, 30);
        self.labTiTle.backgroundColor =[UIColor clearColor];
        self.labTiTle.font = [UIFont systemFontOfSize:17.0];
        self.labTiTle.textAlignment = NSTextAlignmentLeft;
        self.labTiTle.textColor = [UIColor blackColor];
        [self addSubview:self.labTiTle];
        
        //网址
        self.labUrl = [[UILabel alloc]init];
        self.labUrl.frame = CGRectMake(25, 30, kScreenWidth-25, 20);
        self.labUrl.backgroundColor =[UIColor clearColor];
        self.labUrl.font = [UIFont systemFontOfSize:14.0];
        self.labUrl.textAlignment = NSTextAlignmentLeft;
        self.labUrl.textColor = [UIColor blackColor];
        [self  addSubview:self.labUrl];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
