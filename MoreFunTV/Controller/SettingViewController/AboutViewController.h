//
//  AboutViewController.h
//  MoreFunTV
//
//  Created by admin on 14-11-1.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "MatchstickDeviceController.h"
#import <Matchstick/Flint.h>

@interface AboutViewController : BaseViewController
{
    NSString *_app_id;
    BOOL _flagBanBen;
    UITextView *_txtViewDetails;
    BOOL _reslut;
    UILabel *_labVersions;
}
@end
