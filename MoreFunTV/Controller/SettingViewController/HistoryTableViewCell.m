//
//  HistoryTableViewCell.m
//  MoreFunTV
//
//  Created by admin on 2014-10-24.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "HistoryTableViewCell.h"

@implementation HistoryTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _vidioImage =[[UIImageView alloc]init];
        _vidioImage.frame = CGRectMake(25, 5, 90, 125);
        _vidioImage.backgroundColor = [UIColor clearColor];
        //        _vidioImage.layer.borderWidth = 1.1;
        //        _vidioImage.layer.borderColor = [[UIColor colorWithWhite:0.649 alpha:1.000]CGColor];
        [self addSubview:_vidioImage];
        
        _labName = [[UILabel alloc]init];
        _labName.frame = CGRectMake(130, 5, kScreenWidth-130-15, 30);
        _labName.backgroundColor = [UIColor clearColor];
        _labName.font = [UIFont boldSystemFontOfSize:18.0f];
        [self addSubview:_labName];
        
        _labTitle = [[UILabel alloc]init];
        _labTitle.frame = CGRectMake(130, 35, kScreenWidth-130-15, 30);
        _labTitle.font = [UIFont boldSystemFontOfSize:16.0f];
        _labTitle.textColor = [UIColor colorWithWhite:0.713 alpha:1.000];
        _labTitle.backgroundColor = [UIColor clearColor];
        _labTitle.text = @"播放时间:";
        [self addSubview:_labTitle];
        
        _labTime = [[UILabel alloc]init];
        _labTime.frame = CGRectMake(130, 65, kScreenWidth-130-15, 30);
        _labTime.backgroundColor = [UIColor clearColor];
        _labTime.font = [UIFont boldSystemFontOfSize:16.0f];
        _labTime.textColor = [UIColor colorWithWhite:0.713 alpha:1.000];
        _labTime.text = @"观看至1分50秒";
        [self addSubview:_labTime];
        
        _btnDetail = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _btnDetail.frame = CGRectMake(130, 100, 60, 30);
        _btnDetail.backgroundColor = [UIColor colorWithRed:0.234 green:0.650 blue:0.947 alpha:1.000];
        [_btnDetail setTitle:MyLocalizedString(@"detail") forState:UIControlStateNormal];
        _btnDetail.titleLabel.font = [UIFont systemFontOfSize:18.0f];
        _btnDetail.layer.borderWidth = 1.0;
        _btnDetail.layer.borderColor = [[UIColor colorWithRed:0.234 green:0.650 blue:0.947 alpha:1.000]CGColor];
        _btnDetail.layer.cornerRadius = 10.0;
        _btnDetail.clipsToBounds = YES;
        [_btnDetail setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self addSubview:_btnDetail];
        
        UIView *viewLine = [[UIView alloc]init];
        viewLine.frame = CGRectMake(14, 0, 2, 135);
        viewLine.backgroundColor = [UIColor colorWithWhite:0.747 alpha:1.000];
        [self addSubview:viewLine];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
