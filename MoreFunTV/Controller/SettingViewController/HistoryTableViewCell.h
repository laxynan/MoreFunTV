//
//  HistoryTableViewCell.h
//  MoreFunTV
//
//  Created by admin on 2014-10-24.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewCell : UITableViewCell
@property(nonatomic,strong)UIImageView *vidioImage;
//第一行文字
@property(nonatomic,strong)UILabel *labName;
//第二行文字
@property(nonatomic,strong)UILabel *labTitle;
//第三行文字
@property(nonatomic,strong)UILabel *labTime;
@property(nonatomic,strong)UIButton *btnDetail;
@end
