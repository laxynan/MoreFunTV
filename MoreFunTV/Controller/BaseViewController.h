//
//  BaseViewController.h
//  MoreFunTV
//
//  Created by admin on 14-10-21.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmModelViewController.h"
#import "MatchstickDeviceController.h"
#import <Matchstick/Flint.h>

@interface BaseViewController : UIViewController<MatchstickControllerDelegate>

@end
