//
//  MatchStickViewController.h
//  MoreFunTV
//
//  Created by admin on 14-11-1.
//  Copyright (c) 2014年 admin. All rights reserved.
//
//  投射播放界面
//

#import <UIKit/UIKit.h>
#import "PlayControlView.h"
#import "DetailInfo.h"
#import "SeriesInfo.h"
#import "Media.h"
#import "MatchstickDeviceController.h"
#import "NetWorkingHelper.h"
#import "RequestParameter.h"
#import "DBDaoHelper.h"
#import "Loading.h"

@interface MatchStickViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,PlayControlViewDelegate,MatchstickControllerDelegate>
{
    UIView      *_viewSelf;
    //影片名称
    UILabel     *_labName;
    //记录影片名称
    NSString    *_labNameString;
    //控制提示
    UILabel     *_labPrompt;
    //控制按钮
    UIButton    *_btnControl;
    //添加控制界面
    UIView      *_viewControl;
    //值显示-总时长
    UILabel     *_labTotalValue;
    //值显示-当前播放时长
    UILabel     *_labCurrlValue;
    //播放列表
    UITableView *_tablePaly;
    BOOL        _flag;
    //按钮的标记
    BOOL        _flagBtn;
    //页面切换的标记
    BOOL        _flagView;
    //滑动手势
    UIPanGestureRecognizer *_panRecognizer;
    NSMutableArray *_arrPaly;
    UIButton       *_btnTouShe;
    NSString       * _strReal;
    float          _pontXStart;
    float          _pontXStop;
    float          _pontYStart;
    float          _pontXLast;
    float          _pontYLast;
    float          _pontYStop;
    float          _zhi;
    float          _sound;
    int            _count;
    UIImageView    *_imgPrompt;
    BOOL           _flagPrompt;
    int            number;
    BOOL           done;
    NSArray        * _arrUrl;//分段url数组
//    UIImageView *_imgSelf;//背景图切换
    NSString *_strPlay;
    float _vlume;
    PlayControlView *_controlView;
    SetUpSingleton *_set;
    int _arrCount;
    BOOL isGcd;
}
@property float deviceVolume;
@property bool deviceMuted;
@property(strong, nonatomic) DetailInfo *mediaToPlay;
@property(strong,nonatomic) SeriesInfo *seriers;
@property (nonatomic, strong) NSString *strAllRealUrl;
/** The media object and when to start playing on dongle. Set this before presenting the view. */
- (void)setMediaToPlay:(DetailInfo *)newMedia withStartingTime:(NSTimeInterval)startTime;
-(id)initWithRealUrl:(NSString *)url;
@end
