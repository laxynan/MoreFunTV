//
//  VideoTableViewController.m
//  MoreFunTV
//
//  Created by admin on 14-10-22.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "VideoTableViewController.h"
#import "AppDelegate.h"
#import <Matchstick/Flint.h>

@interface VideoTableViewController ()
{
    __weak MatchstickDeviceController *_matchstickController;
}
@end

@implementation VideoTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithClassInfo:(VideoInfo *)classInfo
{
    self = [super init];
    if (self) {
        self.classInfo = classInfo;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _arrVoide = [[NSMutableArray alloc]init];//视频信息数组初始化
    
    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    _matchstickController = delegate.matchstickDeviceController;
    //投射图标判断
    if (_matchstickController.deviceScanner.devices.count > 0) {
        self.navigationItem.rightBarButtonItem = _matchstickController.matchstickBarButton;
    }
    
    //标题自定义
    [self customNavTitle];
    
    //条件选择
    [self chooseView];
    
    //视频信息列表
    [self addVideoTable];
    
    //集成刷新控件
    [self setupRefresh];
    
    //加载不到数据时的提示
    [self alertShow];
    
    //请求视频数据
    [self requestVideoInfo];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _matchstickController.delegate = self;
}

- (void)shouldDisplayModalDeviceController {
    //投射按钮调用的方法
    [[NSNotificationCenter defaultCenter] postNotificationName:@"myDevice" object:nil userInfo:nil];
}

- (void)didDiscoverDeviceOnNetwork {
    // Add the Matchstick icon if not present.
    self.navigationItem.rightBarButtonItem = _matchstickController.matchstickBarButton;
}

//标题自定义
-(void)customNavTitle
{
    self.navigationItem.titleView = nil;
    UIButton *btnBack = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, 44)];
    [btnBack setTitle:[NSString stringWithFormat:@"    %@",self.classInfo.strCategoryName] forState:UIControlStateNormal];
    btnBack.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btnBack.titleLabel.font = [UIFont boldSystemFontOfSize:19.0f];
    [btnBack setBackgroundColor:[UIColor clearColor]];
    [btnBack setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(clickRetun) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"return.png"]];
    image.frame = CGRectMake(0, 12, 10, 20);
    image.backgroundColor = [UIColor clearColor];
    [btnBack addSubview:image];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem = leftItem;
}

//请求视频数据
-(void)requestVideoInfo
{
    _rep = [[RequestParameter alloc]init];
    _rep.portID = 2;
    _rep.strPageNo = @"1";
    _rep.strPageSize = @"30";
    _rep.strCategoryID = self.classInfo.strCategoryId;
    _rep.strAreaID = @"";
    _rep.strStyleID = @"";
    _rep.strReleasedOnFrom = @"";
    _rep.strReleasedOnTo = @"";
    
    [Loading showLoadingWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{//执行耗时操作
        //请求条件选择数据
        [self addTypeInfoArr];
        //请求视频信息
        NSMutableArray *arr = [NetWorkingHelper getCategoryVoideInfoWithRequestParameter:_rep];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //主线程更新UI
            if (arr == nil) {
                [UIView animateWithDuration:0.2f animations:^{
                    [_viel setHidden:NO];
                }];
                [Loading hiddonLoadingWithView:self.view];
                return ;
            }
            [_arrVoide removeAllObjects];
            [_arrVoide addObject:arr];
            [Loading hiddonLoadingWithView:self.view];
            [_tableVideo reloadData];
        });
    });
}

//加载不到数据时的提示
-(void)alertShow
{
    _viel = [[UIView alloc]initWithFrame:CGRectMake(0, 64+50, kScreenWidth, kScreenHeight-50-64)];
    _viel.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
    _viel.clipsToBounds = YES;
    [_viel setUserInteractionEnabled:YES];
    [_viel setHidden:YES];
    [self.view addSubview:_viel];
    
    //提示图片
    UIImageView *imgPrompt = [[UIImageView alloc]initWithFrame:CGRectMake((kScreenWidth-40)/2, 64+(kScreenHeight-48-64)/3-90, 40, 40)];
    [imgPrompt setImage:[UIImage imageNamed:@"dggddhgdhgd.png"]];
    imgPrompt.layer.borderWidth = 1.0;
    imgPrompt.layer.borderColor = [[UIColor clearColor]CGColor];
    imgPrompt.clipsToBounds = YES;
    imgPrompt.layer.cornerRadius = 20.0;
    [_viel addSubview:imgPrompt];
    
    UILabel *labTiShi = [[UILabel alloc]initWithFrame:CGRectMake(0, 64+(kScreenHeight-48-64)/3-50, kScreenWidth, 30)];
    labTiShi.textColor = [UIColor blackColor];
    labTiShi.text = @"未加载到数据";
    labTiShi.font = [UIFont boldSystemFontOfSize:18.0f];
    labTiShi.textAlignment = NSTextAlignmentCenter;
    labTiShi.backgroundColor = [UIColor clearColor];
    [_viel addSubview:labTiShi];
    
    UILabel *labCheck = [[UILabel alloc]initWithFrame:CGRectMake(0, 64+(kScreenHeight-48-64)/3-20, kScreenWidth, 30)];
    labCheck.textColor = [UIColor lightGrayColor];
    labCheck.text = @"请检查网络状况";
    labCheck.font = [UIFont boldSystemFontOfSize:18.0f];
    labCheck.textAlignment = NSTextAlignmentCenter;
    labCheck.backgroundColor = [UIColor clearColor];
    [_viel addSubview:labCheck];
    
    //刷新按钮
    UIButton *btnRefresh = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnRefresh.frame = CGRectMake(kScreenWidth/3, 64+(kScreenHeight-48-64)/3+30, kScreenWidth/3, 40);
    btnRefresh.backgroundColor = [UIColor lightGrayColor];
    btnRefresh.layer.borderWidth = 1.0;
    btnRefresh.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    btnRefresh.layer.cornerRadius = 10.0;
    btnRefresh.clipsToBounds = YES;
    btnRefresh.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [btnRefresh setTitle:MyLocalizedString(@"RefreshC") forState:UIControlStateNormal];
    [btnRefresh setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnRefresh addTarget:self action:@selector(clickRefresh) forControlEvents:UIControlEventTouchUpInside];
    [_viel addSubview:btnRefresh];
}

//点击刷新的事件
-(void)clickRefresh{
    
    [UIView animateWithDuration:0.2f animations:^{
        [_viel setHidden:YES];
    }];
    
    //请求视频数据
    [self requestVideoInfo];
}


//添加条件选择数据
-(void)addTypeInfoArr
{
    NSString *strClass = @"";
    if (self.classInfo.strCategoryEn.length == 0) {
        NSMutableArray *arrClass = [NetWorkingHelper getClassInfoWith:@"categories"];
        for (int i = 0; i < arrClass.count;i++) {
            VideoInfo *vInfo = [arrClass objectAtIndex:i];
            if ([vInfo.strCategoryId intValue] == [self.classInfo.strCategoryId intValue]) {
                strClass = vInfo.strCategoryEn;
            }
        }
    }
    else{
        strClass = self.classInfo.strCategoryEn;
    }
    
    _arrYear = [NetWorkingHelper getTypeInfoWithTypeName:[NSString stringWithFormat:@"%@_years",strClass]];
    _arrType = [NetWorkingHelper getTypeInfoWithTypeName:[NSString stringWithFormat:@"%@_styles",strClass]];
    _arrArea = [NetWorkingHelper getTypeInfoWithTypeName:[NSString stringWithFormat:@"%@_areas",strClass]];
    _arrSequence = [NetWorkingHelper getTypeInfoWithTypeName:[NSString stringWithFormat:@"%@_areas",strClass]];
}

//条件选择
-(void)chooseView
{
    //底部的view
    UIView *chooseBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, kScreenWidth, 50)];
    chooseBgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:chooseBgView];
    
    //年份选择按钮
    _btnYear = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnYear.frame = CGRectMake(kScreenWidth/3*0, 0, kScreenWidth/3, 50);
    _btnYear.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [_btnYear setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.000]];
    [_btnYear setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnYear setTitle:MyLocalizedString(@"type1") forState:UIControlStateNormal];
    [_btnYear setTintColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1.000]];
    [_btnYear setImage:[UIImage imageNamed:@"weixuanzhog.png"] forState:UIControlStateNormal];
    [_btnYear setImageEdgeInsets:UIEdgeInsetsMake(0, 65, 0, 0)];//图片位置偏移
    [_btnYear setTitleEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];//Title位置偏移
    _btnYear.tag = 209;
    [_btnYear addTarget:self action:@selector(clickCondition:) forControlEvents:UIControlEventTouchUpInside];
    [chooseBgView addSubview:_btnYear];
    
    //底部的下划线
    _viewYear = [[UIView alloc]initWithFrame:CGRectMake(0, 49.5, kScreenWidth/3, 0.5)];
    _viewYear.backgroundColor = [UIColor colorWithWhite:0.656 alpha:1.000];
    [_btnYear addSubview:_viewYear];
    
    UIView *viewHyear= [[UIView alloc]initWithFrame:CGRectMake(kScreenWidth/3-0.5, 0, kScreenWidth/3-0.5, 50)];
    viewHyear.backgroundColor = [UIColor colorWithWhite:0.656 alpha:1.000];
    [_btnYear addSubview:viewHyear];
    
    //类型选择按钮
    _btnType = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnType.frame = CGRectMake(kScreenWidth/3*1, 0, kScreenWidth/3, 50);
    _btnType.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [_btnType setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.000]];
    [_btnType setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnType setTitle:MyLocalizedString(@"type2") forState:UIControlStateNormal];
    [_btnType setTintColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1.000]];
    [_btnType setImage:[UIImage imageNamed:@"weixuanzhog.png"] forState:UIControlStateNormal];
    [_btnType setImageEdgeInsets:UIEdgeInsetsMake(0, 65, 0, 0)];//图片位置偏移
    [_btnType setTitleEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];//Title位置偏移
    _btnType.tag = 210;
    [_btnType addTarget:self action:@selector(clickCondition:) forControlEvents:UIControlEventTouchUpInside];
    [chooseBgView addSubview:_btnType];
    
    //底部的下划线
    _viewType = [[UIView alloc]initWithFrame:CGRectMake(0, 49.5, kScreenWidth/3, 0.5)];
    _viewType.backgroundColor = [UIColor colorWithWhite:0.656 alpha:1.000];
    [_btnType addSubview:_viewType];
    
    UIView *viewHType= [[UIView alloc]initWithFrame:CGRectMake(kScreenWidth/3-0.5, 0, kScreenWidth/3-0.5, 50)];
    viewHType.backgroundColor = [UIColor colorWithWhite:0.656 alpha:1.000];
    [_btnType addSubview:viewHType];
    
    //地区选择按钮
    _btnArea = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btnArea.frame = CGRectMake(kScreenWidth/3*2, 0, kScreenWidth/3, 50);
    _btnArea.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    [_btnArea setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.000]];
    [_btnArea setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnArea setTitle:MyLocalizedString(@"type3") forState:UIControlStateNormal];
    [_btnArea setTintColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1.000]];
    [_btnArea setImage:[UIImage imageNamed:@"weixuanzhog.png"] forState:UIControlStateNormal];
    [_btnArea setImageEdgeInsets:UIEdgeInsetsMake(0, 65, 0, 0)];//图片位置偏移
    [_btnArea setTitleEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];//Title位置偏移
    _btnArea.tag = 211;
    [_btnArea addTarget:self action:@selector(clickCondition:) forControlEvents:UIControlEventTouchUpInside];
    [chooseBgView addSubview:_btnArea];
    
    //底部的下划线
    _viewArea = [[UIView alloc]initWithFrame:CGRectMake(0, 49.5, kScreenWidth/3, 0.5)];
    _viewArea.backgroundColor = [UIColor colorWithWhite:0.656 alpha:1.000];
    [_btnArea addSubview:_viewArea];
    
    //    //排序选择按钮
    //    UIButton *btnSequence = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    //    btnSequence.frame = CGRectMake(kScreenWidth/4*4, 0, kScreenWidth/4, 50);
    //    btnSequence.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    //    [btnSequence setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.000]];
    //    [btnSequence setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //    [btnSequence setTitle:@"排序" forState:UIControlStateNormal];
    //    [btnSequence setTintColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1.000]];
    //    [btnSequence setImage:[UIImage imageNamed:@"weixuanzhog.png"] forState:UIControlStateNormal];
    //    [btnSequence setImageEdgeInsets:UIEdgeInsetsMake(0, 55, 0, 0)];//图片位置偏移
    //    [btnSequence setTitleEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];//Title位置偏移
    //    btnSequence.tag = 212;
    //    [btnSequence addTarget:self action:@selector(clickCondition:) forControlEvents:UIControlEventTouchUpInside];
    //    [chooseBgView addSubview:btnSequence];
    //
    //    //底部的下划线
    //    UIView *viewSequence = [[UIView alloc]initWithFrame:CGRectMake(0, 49.5, kScreenWidth/4, 0.5)];
    //    viewSequence.backgroundColor = [UIColor colorWithRed:109.0/255.0 green:109.0/255.0 blue:109.0/255.0 alpha:1.000];
    //    [btnSequence addSubview:viewSequence];
}

//视频信息列表
-(void)addVideoTable
{
    //视频信息列表
    _tableVideo = [[UITableView alloc]initWithFrame:CGRectMake(0, 64+50, kScreenWidth, kScreenHeight-64-50) style:UITableViewStylePlain];
    _tableVideo.delegate = self;
    _tableVideo.dataSource = self;
    _tableVideo.bounces = YES;
    [_tableVideo setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_tableVideo setBackgroundColor:[UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000]];
    [self.view addSubview:_tableVideo];
}

//集成刷新控件
-(void)setupRefresh
{
    _voideIdentifier = @"cellIdentifier";
    // 1.注册cell
    [_tableVideo registerClass:[UITableViewCell class] forCellReuseIdentifier:_voideIdentifier];
    
    // 1.下拉刷新(进入刷新状态就会调用self的headerRereshing)
    [_tableVideo addHeaderWithTarget:self action:@selector(headerRereshing)];
    //warning 自动刷新(一进入程序就下拉刷新)
    [_tableVideo headerBeginRefreshing];
    
    // 2.上拉加载更多(进入刷新状态就会调用self的footerRereshing)
    [_tableVideo addFooterWithTarget:self action:@selector(footerRereshing)];
    
    // 设置文字(也可以不设置,默认的文字在MJRefreshConst中修改)
    _tableVideo.headerPullToRefreshText = @"下拉刷新";
    _tableVideo.headerReleaseToRefreshText = @"释放立即刷新";
    _tableVideo.headerRefreshingText = @"正在刷新...";
    
    _tableVideo.footerPullToRefreshText = @"上拉加载更多";
    _tableVideo.footerReleaseToRefreshText = @"释放立即加载";
    _tableVideo.footerRefreshingText = @"正在加载...";
}

//下拉刷新的方法
- (void)headerRereshing
{
    _rep.strPageNo = @"1";//页码变为起始页码
    [Loading showLoadingWithView:self.view];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{//执行耗时操作
        //刷新数据
        NSMutableArray *arr = [NetWorkingHelper getCategoryVoideInfoWithRequestParameter:_rep];
        dispatch_async(dispatch_get_main_queue(), ^{
            //主线程更新UI
            [Loading hiddonLoadingWithView:self.view];
            if (arr == nil) {
                [UIView animateWithDuration:0.2f animations:^{
                    [_viel setHidden:NO];
                }];
                [Loading hiddonLoadingWithView:self.view];
                return ;
            }
            [_arrVoide removeAllObjects];
            [_arrVoide addObject:arr];
            // 刷新表格  2秒后刷新表格UI
            [_tableVideo reloadData];
        });
    });
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [_tableVideo headerEndRefreshing];
}

//上拉加载的方法
- (void)footerRereshing
{
    int page = [_rep.strPageNo intValue]+1;
    SetUpSingleton *set = [SetUpSingleton sharedSetUp];
    if (page > 0 && page <= set.pageNumber)
    {
        _rep.strPageNo = [NSString stringWithFormat:@"%d",page];
        [Loading showLoadingWithView:self.view];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //执行耗时操作
            NSMutableArray *arr = [NetWorkingHelper getCategoryVoideInfoWithRequestParameter:_rep];
            dispatch_async(dispatch_get_main_queue(), ^{
                //主线程更新UI
                [_arrVoide addObject:arr];
                [Loading hiddonLoadingWithView:self.view];
                
                //2秒后刷新UI  刷新表格
                [_tableVideo reloadData];
            });
        });
    }
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [_tableVideo footerEndRefreshing];
}

//行数
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _arrVoide.count;
}

//行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

//列表每行的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *arrResult = [_arrVoide objectAtIndex:indexPath.section];
    if (arrResult.count%3 > 0)
    {
        return 167*(arrResult.count/3+1);
    }
    else
    {
        return 167*arrResult.count/3;
    }
}

//列表每行显示的内容
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //获取cell row ＝ 0 获取不到 进入创建cell
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle: UITableViewCellStyleDefault reuseIdentifier:_voideIdentifier];
    [cell setBackgroundColor:[UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //从视频信息数组的中取出每个分组的视频信息
    NSMutableArray *arrResult = [_arrVoide objectAtIndex:indexPath.section];
    VideoInfoView *viewInfo = [[VideoInfoView alloc]initWithArrVideo:arrResult];
    if (arrResult.count%3 > 0)
    {
        viewInfo.frame = CGRectMake(0, 0, kScreenWidth, 167*(arrResult.count/3+1));
    }
    else
    {
        viewInfo.frame = CGRectMake(0, 0, kScreenWidth, 167*arrResult.count/3);
    }
    viewInfo.delegate = self;
    [cell addSubview:viewInfo];
    
    return cell;
}


//自定义cell的协议方法
-(void)videoInfo:(VideoInfoView *)videoView VideoInfo:(VideoInfo *)videoInfo
{
    DetailViewController *detail = [[DetailViewController alloc]initWithResId:videoInfo.strVideoId];
    detail.strTitle = videoInfo.strTitle;
    detail.strIndex = @"1";
    detail.strSource = @"";
    [self.navigationController pushViewController:detail animated:YES];
}

//返回前一页
-(void)clickRetun
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

//条件选择按钮事件
-(void)clickCondition:(UIButton *)sender
{
    //条件信息表
    [_chooseView removeFromSuperview];
    switch (sender.tag) {
        case 209:
            _chooseView = [[ChooseView alloc]initWithCondition:_arrYear Button:sender];
            //年份按钮
            [_btnYear setTitleColor:[UIColor colorWithRed:63.0/255.0 green:169.0/255.0 blue:245.0/255.0 alpha:1.000] forState:UIControlStateNormal];
            [_btnYear setBackgroundColor:[UIColor whiteColor]];
            [_btnYear setTintColor:[UIColor colorWithRed:0.234 green:0.650 blue:0.947 alpha:1.000]];
            [_btnYear setImage:[UIImage imageNamed:@"xuanzhong.png"] forState:UIControlStateNormal];
            [_viewYear setHidden:YES];
            //类型按钮
            [_btnType setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [_btnType setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.000]];
            [_btnType setTintColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1.000]];
            [_btnType setImage:[UIImage imageNamed:@"weixuanzhog.png"] forState:UIControlStateNormal];
            [_viewType setHidden:NO];
            //地区按钮
            [_btnArea setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [_btnArea setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.000]];
            [_btnArea setTintColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1.000]];
            [_btnArea setImage:[UIImage imageNamed:@"weixuanzhog.png"] forState:UIControlStateNormal];
            [_viewArea setHidden:NO];
            
            break;
        case 210:
            _chooseView = [[ChooseView alloc]initWithCondition:_arrType Button:sender];
            //类型按钮
            [_btnType setTitleColor:[UIColor colorWithRed:63.0/255.0 green:169.0/255.0 blue:245.0/255.0 alpha:1.000] forState:UIControlStateNormal];
            [_btnType setBackgroundColor:[UIColor whiteColor]];
            [_btnType setTintColor:[UIColor colorWithRed:0.234 green:0.650 blue:0.947 alpha:1.000]];
            [_btnType setImage:[UIImage imageNamed:@"xuanzhong.png"] forState:UIControlStateNormal];
            [_viewType setHidden:YES];
            //年份按钮
            [_btnYear setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [_btnYear setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.000]];
            [_btnYear setTintColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1.000]];
            [_btnYear setImage:[UIImage imageNamed:@"weixuanzhog.png"] forState:UIControlStateNormal];
            [_viewYear setHidden:NO];
            //地区按钮
            [_btnArea setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [_btnArea setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.000]];
            [_btnArea setTintColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1.000]];
            [_btnArea setImage:[UIImage imageNamed:@"weixuanzhog.png"] forState:UIControlStateNormal];
            [_viewArea setHidden:NO];
            break;
        case 211:
            _chooseView = [[ChooseView alloc]initWithCondition:_arrArea Button:sender];
            //地区按钮
            [_btnArea setTitleColor:[UIColor colorWithRed:63.0/255.0 green:169.0/255.0 blue:245.0/255.0 alpha:1.000] forState:UIControlStateNormal];
            [_btnArea setBackgroundColor:[UIColor whiteColor]];
            [_btnArea setTintColor:[UIColor colorWithRed:0.234 green:0.650 blue:0.947 alpha:1.000]];
            [_btnArea setImage:[UIImage imageNamed:@"xuanzhong.png"] forState:UIControlStateNormal];
            [_viewArea setHidden:YES];
            //类型按钮
            [_btnType setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [_btnType setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.000]];
            [_btnType setTintColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1.000]];
            [_btnType setImage:[UIImage imageNamed:@"weixuanzhog.png"] forState:UIControlStateNormal];
            [_viewType setHidden:NO];
            //年份按钮
            [_btnYear setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [_btnYear setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.000]];
            [_btnYear setTintColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1.000]];
            [_btnYear setImage:[UIImage imageNamed:@"weixuanzhog.png"] forState:UIControlStateNormal];
            [_viewYear setHidden:NO];
            break;
        default:
            //            _chooseView = [[ChooseView alloc]initWithCondition:_arrSequence Button:sender];
            break;
    }
    _chooseView.delegate = self;
    _chooseView.backgroundColor = [UIColor colorWithWhite:0.098 alpha:0.600];
    _chooseView.frame = CGRectMake(0, 64+50, kScreenWidth, 0);
    [self.view addSubview:_chooseView];
    
    if (_flag == NO) {
        _tagNo = (int)sender.tag;
        [UIView animateWithDuration:0.2f animations:^{
            _chooseView.frame = CGRectMake(0, 64+50, kScreenWidth, kScreenHeight-64-50);
        }];
        _flag = YES;
    }
    else
    {
        _tagYes = (int)sender.tag;
        _chooseView.frame = CGRectMake(0, 64+50, kScreenWidth, 0);
        
        if (_tagYes != _tagNo){
            _tagNo = (int)sender.tag;
            _chooseView.frame = CGRectMake(0, 64+50, kScreenWidth, kScreenHeight-64-50);
            _flag = YES;
        }
        else{
            _flag = NO;
            [sender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [sender setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.000]];
            [sender setTintColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1.000]];
            [sender setImage:[UIImage imageNamed:@"weixuanzhog.png"] forState:UIControlStateNormal];
            switch (sender.tag) {
                case 209:
                    [_viewYear setHidden:NO];
                    break;
                case 210:
                    [_viewType setHidden:NO];
                    break;
                default:
                    [_viewArea setHidden:NO];
                    break;
            }
        }
    }
}

//条件选项的协议
-(void)chooseView:(ChooseView *)chooseView Condition:(VideoInfo *)condition Button:(UIButton *)btn
{
    [btn setTitle:condition.strCategoryName forState:UIControlStateNormal];
    _chooseView.frame = CGRectMake(0, 64+50, kScreenWidth, 0);
    
    _flag = NO;
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.000]];
    [btn setTintColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1.000]];
    [btn setImage:[UIImage imageNamed:@"weixuanzhog.png"] forState:UIControlStateNormal];
    switch (btn.tag) {
        case 209:
            [_viewYear setHidden:NO];
            break;
        case 210:
            [_viewType setHidden:NO];
            break;
        default:
            [_viewArea setHidden:NO];
        break;
    }
    
    _rep.strPageNo = @"1";
    if (btn.tag == 209) {
        if ([condition.strCategoryId intValue] == 0)
        {
            _rep.strReleasedOnFrom =@"";
            _rep.strReleasedOnTo = @"";
        }
        else
        {
            _rep.strReleasedOnFrom = condition.strCategoryId;
            _rep.strReleasedOnTo = [NSString stringWithFormat:@"%d",[condition.strCategoryId intValue]+1];
        }
    }
    else if (btn.tag == 210){
        if ([condition.strCategoryId intValue] == 0)
        {
            _rep.strStyleID = @"";
        }
        else
        {
            _rep.strStyleID = condition.strCategoryId;
        }
    }
    else if (btn.tag == 211){
        if ([condition.strCategoryId intValue] == 0)
        {
            _rep.strAreaID = @"";
        }
        else
        {
            _rep.strAreaID = condition.strCategoryId;
        }
    }
    else{
    }
    
    [Loading showLoadingWithView:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //执行耗时操作
        //按条件请求数据
        NSMutableArray *arr = [NetWorkingHelper getCategoryVoideInfoWithRequestParameter:_rep];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //主线程更新UI
            [Loading hiddonLoadingWithView:self.view];
            if (arr.count == 0) {
//                NSLog(@"未搜索到相关信息");
            }
            [_arrVoide removeAllObjects];
            [_arrVoide addObject:arr];
            [_tableVideo reloadData];
            _tableVideo.contentOffset = CGPointMake(0, 0);
        });
    });
}

//点击事件的协议方法
-(void)tapRecognizerChooseView:(ChooseView *)chooseView Flag:(BOOL)flag Button:(UIButton *)btn
{
    if (flag == YES) {
        [UIView animateWithDuration:0.2f animations:^{
            _chooseView.frame = CGRectMake(0, 64+50, kScreenWidth, 0);
        }];
        
        _flag = NO;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.000]];
        [btn setTintColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1.000]];
        [btn setImage:[UIImage imageNamed:@"weixuanzhog.png"] forState:UIControlStateNormal];
        switch (btn.tag) {
            case 209:
                [_viewYear setHidden:NO];
                break;
            case 210:
                [_viewType setHidden:NO];
                break;
            default:
                [_viewArea setHidden:NO];
                break;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
