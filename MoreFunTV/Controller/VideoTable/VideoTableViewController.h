//
//  VideoTableViewController.h
//  MoreFunTV
//
//  Created by admin on 14-10-22.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ChooseView.h"
#import "VideoInfoView.h"
#import "DetailViewController.h"
#import "NetWorkingHelper.h"
#import "Loading.h"
#import "MJRefresh.h"
#import "MatchStickViewController.h"

@interface VideoTableViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,ChooseViewDelegate,VideoInfoViewDelegate,MatchstickControllerDelegate>
{
    UIView *_viel;
    NSMutableArray *_arrYear;//年份
    NSMutableArray *_arrType;//类型
    NSMutableArray *_arrArea;//地区
    NSMutableArray *_arrSequence;//排序
    UIButton       *_btnYear;//年份选择按钮
    UIView         *_viewYear;
    UIButton       *_btnType;//类型选择按钮
    UIView         *_viewType;
    UIButton       *_btnArea;//地区选择按钮
    UIView         *_viewArea;
    ChooseView     *_chooseView;
    UITableView    *_tableVideo;//视频信息列表
    BOOL           _flag;//页面标记
    int            _tagYes;//按钮标记
    int            _tagNo;//按钮标记
    NSMutableArray *_arrVoide;//视频信息数组的数组
    RequestParameter *_rep;//请求参数
    NSString *_voideIdentifier;
}
@property(nonatomic,retain) VideoInfo *classInfo;
- (id)initWithClassInfo:(VideoInfo *)classInfo;
@end
