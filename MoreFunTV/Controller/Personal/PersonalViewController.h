//
//  PersonalViewController.h
//  MoreFunTV
//
//  Created by admin on 14-10-21.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Loading.h"
#import "AFNetworking.h"
#import "MatchStickViewController.h"
#import "AlertCustomView.h"

@interface PersonalViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,MatchstickControllerDelegate,AlertCustomViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_arrTitle;//标题title
    NSMutableArray *_arrHistory;//历史记录
    int _sectionFlag;
    DetailInfo *_detail;
    NSString  *_strUrlWebView;//网页地址
    NSString  *_strRUrl;//投射地址
    NSMutableArray *_arrSheet;
    NSString *_strRealUrlAll;
    NSDictionary *_urlDic;
    NSArray *_arr1;
    NSArray *_arr2;
    NSArray *_arr3;
    NSArray *_arr4;
    BOOL _pushmatst;
    BOOL _webPlay;
    AlertCustomView *_cusAlterView;
}
@end
