//
//  PersonalViewController.m
//  MoreFunTV
//
//  Created by admin on 14-10-21.
//  Copyright (c) 2014年 admin. All rights reserved.
//

#import "PersonalViewController.h"
#import "HistoryTableViewCell.h"
#import "DetailViewController.h"
#import "SettingViewController.h"
#import "WebviewViewController.h"
#import "AppDelegate.h"
#import <Matchstick/Flint.h>
#import "static.h"
#import "lib/include/libanalysis/libanalysis.h"
#import "UIImageView+AFNetworking.h"

libanalysis *libabbbUrl = [[libanalysis alloc]init];
@interface PersonalViewController ()
{
    int lastKnownPlaybackTime;
    __weak MatchstickDeviceController *_matchstickController;
}
@property MPMoviePlayerController *moviePlayer;
@end

@implementation PersonalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    _matchstickController = delegate.matchstickDeviceController;
    if (_matchstickController.deviceScanner.devices.count > 0) {
        self.tabBarController.navigationItem.rightBarButtonItem = _matchstickController.matchstickBarButton;
    }
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    _cusAlterView = [[AlertCustomView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    _cusAlterView.delegate = self;
    _cusAlterView.backgroundColor = [UIColor colorWithWhite:0.098 alpha:0.600];
    _cusAlterView.alpha = 0;
    [app.window addSubview:_cusAlterView];
    
    //用户信息view
    [self createLogView];
    
    [self creatHistoryView];
    //创建列表
    [self createTableView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 44)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:19.0f];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = MyLocalizedString(@"yonghu");
    self.tabBarController.navigationItem.titleView = titleLabel;
    
    _matchstickController.delegate = self;
    
    //初始化
    NSMutableArray *arrToday = [[NSMutableArray alloc]init];//今天
    NSMutableArray *arrWeek = [[NSMutableArray alloc]init];//七天内
    NSMutableArray *arrMoth = [[NSMutableArray alloc]init];//一个月内
    NSMutableArray *arrEarly = [[NSMutableArray alloc]init];//更早
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{//执行耗时操作
        //查询数据库
        NSMutableArray *arr = [DBDaoHelper selectAllPlayInfo];
        dispatch_async(dispatch_get_main_queue(), ^{//主线程更新UI
            for (int i = 0; i < arr.count; i++) {
                DetailInfo *detail = [arr objectAtIndex:i];
                //不是从添加列表方式存入数据库
                if (![detail.strPlaySource isEqualToString:@"addList"]) {
                    NSDate *datenow = [NSDate date];
//                    NSComparisonResult result2 = [datenow timeIntervalSinceDate:detail.dateTime] ;
                    NSTimeInterval res = [datenow timeIntervalSinceDate:detail.dateTime ];
                    double cha= res/60/60/24 ;
                    
                    if (cha <= 1) {//当天
                        [arrToday addObject:detail];
                    }else if (cha > 1 && cha<=7){//七天以内
                        [arrWeek addObject:detail];
                    }else if (cha > 7 && cha<=30){//一个月内
                        [arrMoth addObject:detail];
                    }else if(cha > 30){//更早
                        [arrEarly addObject:detail];
                    }
                }
            }
            
            _arrHistory = [[NSMutableArray alloc]init];//播放记录
            _arrTitle = [[NSMutableArray alloc]init];//标题
            
            if (arrToday.count != 0) {
                [_arrHistory addObject:arrToday];
                [_arrTitle addObject:@"今天"];
            }
            else if (arrWeek.count != 0) {
                [_arrHistory addObject:arrWeek];
                [_arrTitle addObject:@"七天内"];
            }
            else if (arrMoth.count != 0) {
                [_arrHistory addObject:arrMoth];
                [_arrTitle addObject:@"一个月内"];
            }
            else if (arrEarly.count != 0) {
                [_arrHistory addObject:arrEarly];
                [_arrTitle addObject:@"更早"];
            }
            [_tableView reloadData];
        });
    });
    
}

- (void)shouldDisplayModalDeviceController {
    //投射按钮调用的方法
    [[NSNotificationCenter defaultCenter] postNotificationName:@"myDevice" object:nil userInfo:nil];
}

- (void)didDiscoverDeviceOnNetwork {
    // Add the Matchstick icon if not present.
    self.tabBarController.navigationItem.rightBarButtonItem = _matchstickController.matchstickBarButton;
}

//Logo展示
-(void)createLogView
{
    UIView * myView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, kScreenWidth, 110)];
    myView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:myView];
    
    //头像
    UIImageView *personView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"LogoMoreFunTV.png"]];
    personView.frame = CGRectMake(0, 0, kScreenWidth, 110);
    [myView addSubview:personView];

    UIButton *btnSet = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnSet.frame = CGRectMake(kScreenWidth-55, 8 , 50, 50);
    btnSet.backgroundColor = [UIColor clearColor];
    btnSet.tintColor = [UIColor whiteColor];
    [btnSet setImage:[UIImage imageNamed:@"setup.png"] forState:UIControlStateNormal];
    [btnSet addTarget:self action:@selector(btnSet) forControlEvents:UIControlEventTouchUpInside];
    [myView addSubview:btnSet];
    
}

//设置按钮
-(void)btnSet
{
    SettingViewController *setView =[[SettingViewController alloc]init];
    [self.navigationController pushViewController:setView animated:YES  ];
}
//历史记录
-(void)creatHistoryView
{
    UIView *historyView = [[UIView alloc]initWithFrame:CGRectMake(0, 174, kScreenWidth, 40)];
    historyView.backgroundColor = [UIColor colorWithRed:204/255.0 green:204/255.0  blue:204/255.0  alpha:1.000];
    [self.view addSubview:historyView];
    
    UILabel *historyLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 40)];
    historyLab.backgroundColor = [UIColor clearColor];
    historyLab.text = MyLocalizedString(@"playHistory");
    historyLab.textAlignment = NSTextAlignmentLeft;
    historyLab.font = [UIFont systemFontOfSize:18.0f];

    [historyView addSubview:historyLab];
    
    UIButton *btnClean = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnClean.frame = CGRectMake(kScreenWidth-15-40, 0 , 40, 40);
    btnClean.tintColor = [UIColor colorWithWhite:0.472 alpha:1.000];
    btnClean.backgroundColor = [UIColor clearColor];
    [btnClean setImage:[UIImage imageNamed:@"clean.png"] forState:UIControlStateNormal];
    [btnClean addTarget:self action:@selector(btnClean) forControlEvents:UIControlEventTouchUpInside];
    [historyView addSubview:btnClean];
}
//清空播放记录
- (void)btnClean
{
    _webPlay = NO;
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfod") delegate:self cancelButtonTitle:MyLocalizedString(@"cancel") otherButtonTitles:MyLocalizedString(@"sure"), nil];
    [alert show];
}

-(void)createTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64+110+40, kScreenWidth, kScreenHeight-64-110-40-48)style:UITableViewStyleGrouped];
    _tableView.separatorColor = [UIColor clearColor];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
    [self.view addSubview:_tableView];
}
//tableview 的分组
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _arrHistory.count;
}

//设置headertitle的view
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *lab = [[UILabel alloc]init];
    lab.textAlignment = NSTextAlignmentLeft;
    lab.text =[ NSString stringWithFormat:@"       %@",[_arrTitle objectAtIndex:section]];
    lab.font = [UIFont systemFontOfSize:17.0f];
    lab.backgroundColor = [UIColor clearColor];
    
    if (section == 0) {
    }
    else{
        UIView *viewN = [[UIView alloc]init];
        viewN.frame = CGRectMake(14, 0, 2, 12);
        viewN.backgroundColor = [UIColor colorWithWhite:0.747 alpha:1.000];
        [lab addSubview:viewN];
    }
    
    //蓝色标识
    UIImageView *imgA = [[UIImageView alloc]init];
    imgA.frame = CGRectMake(7, 12, 16, 16);
    if (section == 0) {
        [imgA setImage:[UIImage imageNamed:@"today.png"]];
    }else{
        [imgA setImage:[UIImage imageNamed:@"past.png"]];
    }
    imgA.backgroundColor = [UIColor clearColor];
    [lab addSubview:imgA];
    
    UIView *viewM = [[UIView alloc]init];
    viewM.frame = CGRectMake(14, 28, 2, 12);
    viewM.backgroundColor = [UIColor colorWithWhite:0.747 alpha:1.000];
    [lab addSubview:viewM];
    return lab;
}

//每个分组的cell个数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray *arrResult = [_arrHistory objectAtIndex:section];
    return arrResult.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"firstTable";
    int section = [indexPath section];
    int row = [indexPath row];
    
    HistoryTableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:identifier];
    //判断是否有被隐藏的cell  如果没有就创建一个
    if (cell == nil) {
        cell = [[HistoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.000];
    }
    
    NSMutableArray *arrResult = [_arrHistory objectAtIndex:section];
    _sectionFlag = section;
    DetailInfo *detail = [arrResult objectAtIndex:row];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *strDate = [dateFormatter stringFromDate:detail.dateTime];
    //没有获取到播放时间
    if ([detail.strHistoryTime isEqualToString:@""]) {//0分0秒"]) {
        cell.labTitle.text = @"播放时间:";
        cell.labTime.text = strDate;
    }
    else {
        cell.labTitle.text = strDate;
        NSArray *hms = [detail.strHistoryTime componentsSeparatedByString:@":"];
        NSString *hmsString = @"";
        int j = 0;
        for (int i = hms.count-1; i >= 0; i --) {
            //值不为0
            if (![@"0" isEqualToString:hms[i]]) {
                j ++;
                //选择时间单位
                switch (j) {
                    case 1:
                        hmsString = [NSString stringWithFormat:@"%@秒%@",hms[i],hmsString];
                        break;
                    case 2:
                        hmsString = [NSString stringWithFormat:@"%@分%@",hms[i],hmsString];
                        break;
                    case 3:
                        hmsString = [NSString stringWithFormat:@"%@时%@",hms[i],hmsString];
                        break;
                    default:
                        hmsString = [NSString stringWithFormat:@"%@ %@",hms[i],hmsString];
                        break;
                }
            }
        }
        cell.labTime.text = [NSString stringWithFormat:@"观看至 %@", hmsString];
    }
    cell.labName.text = detail.strTitle;
    [cell.btnDetail addTarget:self action:@selector(clickDetail:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnDetail.tag = 10222+row;
    [cell.vidioImage setImageWithURL:[NSURL URLWithString:detail.strThumbnail] placeholderImage:[UIImage imageNamed:@"poster_default.png"]];
    return cell;
}

//详情按钮的点击方法
-(void)clickDetail:(UIButton *)sender
{
    NSMutableArray *arrResult = [_arrHistory objectAtIndex:_sectionFlag];
    DetailInfo *de = [arrResult objectAtIndex:sender.tag-10222];
    DetailViewController *detail = [[DetailViewController alloc]initWithResId:de.strResId];
    detail.strTitle = de.strTitleString;
    detail.strIndex = de.strIndex;
    detail.strSource = de.strSource;
    [self.navigationController pushViewController:detail animated:YES];
}

//cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 135;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *arrResult = [_arrHistory objectAtIndex:indexPath.section];
    _detail = [arrResult objectAtIndex:indexPath.row];
    if (_detail.strResUrl.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfo9") delegate:nil cancelButtonTitle:MyLocalizedString(@"sure") otherButtonTitles:nil];
        [alert show];
    }
    else{
        _strUrlWebView = _detail.strResUrl;
        AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [Loading showLoadingWithView:app.window];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{//执行耗时操作
            //调用网址解析获取视频文件地址
            [self touShe:_strUrlWebView];
            dispatch_async(dispatch_get_main_queue(), ^{//主线程更新UI 
                [Loading hiddonLoadingWithView:app.window];
                if (_strRUrl.length == 0) {
                    _webPlay = YES;
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfoa") delegate:self cancelButtonTitle:MyLocalizedString(@"cancel") otherButtonTitles:MyLocalizedString(@"alertinfob"),nil];
                    [alert show];
                }
                else{
                    if (_matchstickController.deviceScanner.devices.count > 0) {
                        //调用设备选择页面
                        [self addAction];
                    }
                    else{
                        //网页播放
                        [self webPlay];
                    }
                }
            });
        });
    }
}

-(void)touShe:(NSString *)strUrl{
    //网址解析
    const LPCSTR url = [strUrl cStringUsingEncoding:NSASCIIStringEncoding];
    [libabbbUrl URLTIsRunning];
    const int sourceId = 0;
    char realURL[1024*30];// = new char(1024*30);
    int len = 1024*30;
    const LPCSTR ua = "";
    const LPCSTR sourceKind = "";
    const LPCSTR spiderName = "";
    const LPCSTR videoParam = "";
    [libabbbUrl AnalysisRealUrlForAndroidMorefun: sourceId url:url realurl:realURL len:len ua:ua skind:sourceKind spiderName:spiderName videoParam:videoParam];
    _strRealUrlAll = [NSString stringWithUTF8String:realURL];
    if (_strRealUrlAll.length != 0){
        _urlDic = [NSJSONSerialization JSONObjectWithData: [_strRealUrlAll dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: nil];
        _arr1 = [_urlDic objectForKey:@"nd"];
        _arr2 = [_urlDic objectForKey:@"sd"];
        _arr3 = [_urlDic objectForKey:@"hd"];
        _strRUrl = @"";
        if(_arr1.count == 0){
            if (_arr2.count == 0) {
                if (_arr3.count != 0) {
                    _strRUrl = [_arr3 objectAtIndex:0];
                }
            }else{
                _strRUrl = [_arr2 objectAtIndex:0];
            }
        }
        else{
            _strRUrl = [_arr1 objectAtIndex:0];
        }
    }
}

//调用设备选择页面
-(void)addAction{
    _arrSheet = [[NSMutableArray alloc]init];
    if (_matchstickController.isConnected) {
        _cusAlterView.isConnect = YES;
    }
    else{
        _cusAlterView.isConnect = NO;
        for (int i = 0; i < _matchstickController.deviceScanner.devices.count; i++) {
            MSFKDevice *device = [_matchstickController.deviceScanner.devices objectAtIndex:i];
            [_arrSheet addObject:device.friendlyName];
        }
        _cusAlterView.arrCustom = _arrSheet;
    }
    //图标变化
    [_cusAlterView rightBtnImageChange];
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView animateWithDuration:0.25f animations:^{
        _cusAlterView.alpha = 1.0;
    }];
}

//投射按钮协议事件
-(void)customTouSheBtnAlterView:(AlertCustomView *)alter{
    [self touShePlay];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView animateWithDuration:0.25f animations:^{
        _cusAlterView.alpha = 0.0;
    }];
}

//网页播放和添加列表按钮协议事件
-(void)customWebORAddBtnAlterView:(AlertCustomView *)alter{
    if (_cusAlterView.isConnect == NO) {//网页播放
        [self webPlay];
        [_cusAlterView tableChange];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView animateWithDuration:0.25f animations:^{
            _cusAlterView.alpha = 0.0;
        }];
    }
    else{//添加队列
        BOOL result = [DBDaoHelper selectTouShe:_detail.strTitle];
        if (result == YES) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:MyLocalizedString(@"alertinfo1") message:MyLocalizedString(@"alertinfo8") delegate:nil cancelButtonTitle:MyLocalizedString(@"sure") otherButtonTitles:nil];
            [alert show];
        }
        else{
            SetUpSingleton *set = [SetUpSingleton sharedSetUp];
            set.strAddTouShe = @"YES";
            DetailInfo *dea = [[DetailInfo alloc]init];
            dea.strTitle = _detail.strTitle;
            dea.strRealUrl = _strRUrl;
            dea.strAllURL = _strRealUrlAll;
            dea.strPlayId = _detail.strPlayId;
            //插入投射记录信息
            [DBDaoHelper insertTouShe:dea];
        }
        
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView animateWithDuration:0.25f animations:^{
            _cusAlterView.alpha = 0.0;
        }];
    }
}

//cell的点击协议
-(void)cellTouchAlterView:(AlertCustomView *)alter RowAtIndexPath:(NSIndexPath *)indexPath{
    SetUpSingleton *set = [SetUpSingleton sharedSetUp];
    NSMutableArray *deviceArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < _matchstickController.deviceScanner.devices.count; i++) {
        MSFKDevice *device = [_matchstickController.deviceScanner.devices objectAtIndex:i];
        // only show LAN IP.
        if (device.ipAddress == nil
            || [device.ipAddress isEqualToString:@"192.168.1.1"]
            || !([device.ipAddress hasPrefix:@"192"]
                 || [device.ipAddress hasPrefix:@"172"]
                 || [device.ipAddress hasPrefix:@"10"])) {
                //过滤掉的设备
            }
        else {
            [deviceArr addObject:device];
        }
    }
    MSFKDevice *device = [deviceArr objectAtIndex:indexPath.row];
    NSLog(@"Selecting device4:%@", device.friendlyName);
    [_matchstickController connectToDevice:device];
    NSString *str = [_arrSheet objectAtIndex:[indexPath row]];
    set.arrSheet = [[NSMutableArray alloc]init];
    [set.arrSheet addObject:str];
    
    _pushmatst = YES;
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView animateWithDuration:0.25f animations:^{
        _cusAlterView.alpha = 0.0;
    }];
}

//点击事件的协议
-(void)tapRecognizerAlterView:(AlertCustomView *)alterView{
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView animateWithDuration:0.25f animations:^{
        [_cusAlterView tableChange];
        _cusAlterView.alpha = 0.0;
    }];
}

- (void)didConnectToDevice:(MSFKDevice *)device {
    lastKnownPlaybackTime = [self.moviePlayer currentPlaybackTime];
    [self.moviePlayer stop];
    if (_pushmatst == YES) {
        _pushmatst = NO;
        [self touShePlay];
    }
}

-(void)touShePlay{
    SetUpSingleton *set = [SetUpSingleton sharedSetUp];
    //隐藏浮动的按钮
    set.strHidden = @"YES";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BtnSetHidden" object:nil userInfo:nil];
    UIButton *matchstickButton = (UIButton *) _matchstickController.matchstickBarButton.customView;
    [matchstickButton setImage:[UIImage imageNamed:@"playing.png"] forState:UIControlStateNormal];
//    NSLog(@"_____________playing.png");
    
    set.strAddTouShe = @"NO";
    set.count = 0;
    DetailInfo *detail = [[DetailInfo alloc]init];
    detail.strTitle = _detail.strTitle;
    detail.strRealUrl = _strRUrl;
    detail.strAllURL = _strRealUrlAll;
    detail.strThumbnail = _detail.strThumbnail;
    //查询投射信息是否已经插入
    if (![DBDaoHelper selectTouShe:detail.strTitle]) {
        detail.strPlayId = _detail.strPlayId;
        //插入投射记录信息
        [DBDaoHelper insertTouShe:detail];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateHistoryPlay" object:nil userInfo:nil];
    //投射播放
    MatchStickViewController *mat = [[MatchStickViewController alloc]initWithRealUrl:_strRUrl];
    mat.mediaToPlay = _detail;
    set._titleString = _detail.strTitle;
//    NSLog(@"_______________%@",[SetUpSingleton sharedSetUp]._titleString);
    mat.strAllRealUrl = _strRealUrlAll;
    [self presentViewController:mat animated:YES completion:^{
    }];
}

-(void)webPlay
{
    //网页播放视频
    WebviewViewController *we = [[WebviewViewController alloc]initWithHtmlUrl:_strUrlWebView];
    [self presentViewController:we animated:YES completion:^{
    }];
}

//清空历史记录
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        if (_webPlay == YES) {
            //网页播放
            [self webPlay];
        }
        else{
            //删除播放记录
            [DBDaoHelper deleteAllTABLEPlay];
            [self viewWillAppear:YES];
            [_tableView reloadData];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
