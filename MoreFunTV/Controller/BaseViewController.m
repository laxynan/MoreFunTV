//
//  BaseViewController.m
//  MoreFunTV
//
//  Created by admin on 14-10-21.
//  Copyright (c) 2014????? admin. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"

@interface BaseViewController (){
   __weak MatchstickDeviceController *_matchstickDeviceController; 
}

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIView *viewDiBu = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 64)];
    viewDiBu.backgroundColor = [UIColor colorWithWhite:0.280 alpha:1.000];
    [self.view addSubview:viewDiBu];
    
    //创建
    [self customNav];
}

//创建navigation上得控件
-(void)customNav
{
    //网页模式
    UIButton *btnWeb = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 65, 22)];
    [btnWeb setTitle:MyLocalizedString(@"wangye") forState:UIControlStateNormal];
    btnWeb.titleLabel.font = [UIFont systemFontOfSize:14.50f];
    [btnWeb setBackgroundImage:[UIImage imageNamed:@"webView1.png"] forState:UIControlStateNormal];
    [btnWeb setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnWeb addTarget:self action:@selector(clickWeb) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:btnWeb];
    self.tabBarController.navigationItem.leftBarButtonItem = leftItem;
    
    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    _matchstickDeviceController = delegate.matchstickDeviceController;
    // Display fling icon in the right nav bar button, if we have devices.
    if (_matchstickDeviceController.deviceScanner.devices.count > 0) {
        self.navigationItem.rightBarButtonItem = _matchstickDeviceController.matchstickBarButton;
        self.tabBarController.navigationItem.rightBarButtonItem = _matchstickDeviceController.matchstickBarButton;
    }
}

//进入网页模式页面
-(void)clickWeb
{
    FilmModelViewController *film = [[FilmModelViewController alloc]init];
    [self.navigationController pushViewController:film animated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    _matchstickDeviceController.delegate = self;
}

- (void)didDiscoverDeviceOnNetwork {
    // Add the Matchstick icon if not present.
    self.navigationItem.rightBarButtonItem = _matchstickDeviceController.matchstickBarButton;
    self.tabBarController.navigationItem.rightBarButtonItem = _matchstickDeviceController.matchstickBarButton;
}


/**
 * Called to display the modal device view controller from the fling icon.
 */

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
